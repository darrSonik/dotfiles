# If not running interactively, don't do anything
# [[ $- == *i* ]] && source /home/asad/.local/share/blesh/ble.sh --noattach --rcfile $HOME/.config/blesh/blerc
# Shell Customisation {{{
# Identify if it's login shell
#_login_status="$(shopt login_shell | awk '{print $2}')"

# Disable Ctrl-s and Ctrl-q
#stty -ixon

# Prompt
git_stat() {
  STATUS="$(git status 2> /dev/null)"
  if [[ $? -ne 0 ]]; then printf ""; return; else printf "$(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1/')"; fi
  if echo ${STATUS} | grep -c "nothing to commit"  &> /dev/null; then printf ""; else printf " "; fi
  if echo ${STATUS} | grep -c "renamed:"           &> /dev/null; then printf ">"; else printf ""; fi
  if echo ${STATUS} | grep -c "branch is ahead:"   &> /dev/null; then printf "!"; else printf ""; fi
  if echo ${STATUS} | grep -c "new file:"          &> /dev/null; then printf "+"; else printf ""; fi
  if echo ${STATUS} | grep -c "Untracked files:"   &> /dev/null; then printf "?"; else printf ""; fi
  if echo ${STATUS} | grep -c "modified:"          &> /dev/null; then printf "*"; else printf ""; fi
  if echo ${STATUS} | grep -c "deleted:"           &> /dev/null; then printf "-"; else printf ""; fi
  printf ")"
}

bg_jobs() {
    job_count=$(jobs | wc -l)
    [ $job_count -gt 0 ] && printf ' [bg: %d]' $job_count
}

tasks_to_do() {
    # TODO: implement this using taskwarrior
    printf " 🤪"
}

if [ $TERM = "linux" ]
then
    PS1='\n\e[93m\u\e[0m@\e[94m\H\e[0m in \e[35m$PWD\e[90m$(git_stat)\e[0m\e[31m$(bg_jobs)\e[0m\n └> '
else
    [ -z "$TMUX" ] && PS1='\n\e[93;49m\e[30;103m \u \e[93;104m\e[30;104m \H \e[94;45m\e[30;45m $PWD \e[35;49m\e[0m\e[90m$(git_stat)\e[0m\e[31m$(bg_jobs)\e[0m\n   └⮞ ' || PS1='\n\e[90m$(git_stat)\e[31m$(bg_jobs) \e[96m⮞\e[0m '
fi

#[ -z "$TMUX" ] && PS1='\n\e[93m\u\e[0m@\e[94m\H\e[0m in \e[35m$PWD\e[90m$(git_stat)\e[0m\e[31m$(bg_jobs)\e[0m\n └⮞ ' || PS1='\n \e[96m⮞\e[0m '
#[ -z "$TMUX" ] && PS1='\n\e[93m\u\e[0m@\e[94m\H\e[0m in \e[35m\w\e[90m$(git_stat)\e[0m \e[31m$(bg_jobs)\e[0m\n └⮞ ' || PS1='\n \e[96m⮞\e[0m '
#[ -z "$TMUX" ] && PS1='\n\e[93m\u\e[0m@\e[94m\H\e[0m in \e[35m$PWD\e[m$(git_stat)\e[m\n └⮞ ' || PS1='\n\e[90m$(git_stat) \e[92m⮞\e[0m '
#PS1='\n\e[35mbg:\j\e[m \e[93mcmd:\!\e[0m \e[94m⮞\e[0m '

# History
HISTFILE=~/.local/share/bash_history
HISTSIZE=
HISTFILESIZE=
HISTCONTROL=ignoreboth:erasedups
#HISTIGNORE="&:ls:[bf]g:devour*:lsd*:exit:pwd:clear:sudo mount*:sudo umount*:history*:tm:tk:up*:lfcd*:taskmgr:[ \t]*"
HISTIGNORE="dotc:exit:pwd:clear:history*:ls:cd:cd ..:cd -:lf*:open:manl:termdown*:btd:vmd*:vpn*:j[fbd]:jobs:devour"

# Shell Options
shopt -s cmdhist
shopt -s checkjobs
shopt -s checkwinsize
shopt -s dirspell
shopt -s cdspell
shopt -s extglob

# To share history among sessions
shopt -s histappend
PROMPT_COMMAND='history -a; history -c; history -r; pwd > /media/data-drive/.cache/curr-dirr; printf "\033]0;%q\007" ${PWD##*/}'
#PROMPT_DIRTRIM=5

# Abbreviated current directory. Add ${PS1X} to PS1
#PROMPT_COMMAND='PS1X=$(p="${PWD#${HOME}}"; [ "${PWD}" != "${p}" ] && printf "~";IFS=/; for q in ${p:1}; do printf /${q:0:1}; done; printf "${q:1}")'

#### Set Window Title
function settitle () {
    export PREV_COMMAND=${PREV_COMMAND}${@}
    printf "\033]0;%s\007" "${BASH_COMMAND//[^[:print:]]/}"
    export PREV_COMMAND=${PREV_COMMAND}' | '
}
trap 'settitle "$BASH_COMMAND"' DEBUG

#### Removing the duplicates automatically!!!!
remove_duplicate_history() {
    tac $HISTFILE | awk '!x[$0]++' | tac >| /tmp/bash_hist
    mv /tmp/bash_hist $HISTFILE
}
#trap remove_duplicate_history EXIT
# }}}
# Variables {{{
export PROMPT_COMMAND=${PROMPT_COMMAND}';export PREV_COMMAND=""'

#LOCALPATH="/home/asad/.local/bin:/home/asad/.local/scripts:/home/asad/.yarn/bin"
#[[ ${PATH} = *${LOCALPATH}* ]] || export PATH="${PATH}:${LOCALPATH}"

# Colourful Manpage
export LESS_TERMCAP_mb=$'\e[1;32m'
export LESS_TERMCAP_md=$'\e[1;32m'
export LESS_TERMCAP_me=$'\e[0m'
export LESS_TERMCAP_se=$'\e[0m'
export LESS_TERMCAP_so=$'\e[1;33m'
export LESS_TERMCAP_ue=$'\e[0m'
export LESS_TERMCAP_us=$'\e[1;4;31m'
# }}}
# Aliases {{{
alias ls='ls --color=auto'
alias grep='grep --colour'
alias lfe='lf /run/media/asad'
alias lfE='lfcd /run/media/asad'
alias lfd='lf /media/data-drive'
alias lfD='lfcd /media/data-drive'
alias zf='z -I'
alias zb='z -b'
alias v='nvim'
alias d='devour'
alias dr='dragon-drop'
alias dotf='git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME'
alias dup='rsync -r -u -P'
alias reloc='rsync -r -u --remove-source-files -P'
alias md='mkdir -p'
alias manl='mimeo "/media/data-drive/Documents/manuals/$(ls /media/data-drive/Documents/manuals | fzf --reverse --height=20%)"'
alias music='$HOME/.local/scripts/youtube-music.sh &> /dev/null & disown "%$(jobs | rg -i youtube | head -c 2 | tail -c 1)"'
#alias open='mimeo'
#alias sess="abduco -A $(abduco | awk 'NR>1 {if(NF==5) print $5; else print $4;}' | pmenu -p 'Session: ')"
#alias taskmgr='sakura --class=TaskManager --name=bashtop -t "Task Manager" -x bashtop'
alias nsudoc='cp /media/data-drive/Documents/NSU/latex/* ./ && nvim document.tex'
alias uni='mimeo "/media/data-drive/Pictures/ScreenShots/Varsity Schedule.png"'
alias webmark='rofi -modi webmarks:/home/asad/.local/scripts/browser-bookmarks.py -show webmarks'
alias gc='VISUAL=nvim git commit'
alias cam='gst-launch-1.0 v4l2src device=/dev/video0 ! xvimagesink'
alias fixk='setxkbmap -option caps:swapescape ; xset r rate 200 35'
alias tree='tree -C'
# }}}
# Functions {{{
# Fix PATH duplication on exit
#fix_path() {
#    [ "$_login_status" = "on" ] && source $HOME/.bash_logout || export PATH="$OLD_PATH"
#}
# fix_path() {
#     export PATH="$OLD_PATH"
# }
# Jobs control
job_control() {
    [ $# -ne 1 ] && { echo 'What to do? Kill, Disown, Send to foreground or background?'; return 0; }
    case "$1" in
	'fg'    ) fg %$(jobs | fzf --reverse --height=20% --prompt="Bring to foreground: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'bg'    ) bg %$(jobs | fzf --reverse --height=20% --prompt="Continue in background: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'disown') disown %$(jobs | fzf --reverse --height=20% --prompt="Disown: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'kill'  ) kill $(jobs -l | fzf  --reverse --height=20% --prompt="Kill: " | awk '{print $2}') ;;
	 *      ) echo '1 arg: kill/disown/fg/bg' ;;
    esac
}
alias jf='job_control fg'
alias jb='job_control bg'
alias jd='job_control disown'
alias jk='job_control kill'

# mkdir and cd
mdc() {
    mkdir -p "$1" && cd "$1"
}

# Tree structure with ls
lst() {
    [ $# -gt 1 ] && { echo '0/1 argument (directory)'; return; }
    [ -z "$1" ] && fdcmd="fd . ." || fdcmd="fd . $1"
    fullcmd="$fdcmd | sed -e 's/[^-][^\/]*\// |/g' -e 's/|\([^ ]\)/|- \1/'"
    eval "$fullcmd"
}

# Use lf to switch directories and bind it to ctrl-o
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}

# Search and open with $EDITOR
# vf () {
#     case "$#" in
# 	1) filename=$(fd -H -t f . "$1" | fzf --exact --multi --ansi --reverse --height=50% --preview 'bat --color=always {}' --preview-window=right:50% | tr '\n' ' ');;
# 	0) filename=$(fd -H -t f . . | fzf --exact --multi --ansi --reverse --height=50% --preview 'bat --color=always {}' --preview-window=right:50% | tr '\n' ' ');;
# 	*) echo 'Run without argument or directory to search';;
#     esac
#     [ -n "$filename" ] && $EDITOR $filename || echo 'Aborted'
# }
alias vfc="vf ~/.config"
alias vfs="vf ~/.local/scripts"

# Start predifined TMUX session
tm() {
    local _sess=$(tmuxstart -l | fzf --reverse --height=30%)
    [ -z $_sess ] && echo "No session started" || tmuxstart $_sess
}

# Open last added file
open_last() {
    local file_name="$(ls -t | head -9 | fzf --reverse --height=30% --prompt='Open: ')"
    [ -z "$file_name" ] && echo 'Aborted' || mimeo "$file_name"
}

# Move directories with rsync (TODO:update this)
relocd() {
    rsync -r -u --remove-source-files -P -v "$@"
    find $1 -depth -type d -empty -exec rmdir "{}"
}

# codi() {
#   local syntax="${1:-python}"
#   shift
#   nvim -c \
#     "let g:startify_disable_at_vimenter = 1 |\
#     set bt=nofile ls=0 noru nonu nornu |\
#     hi ColorColumn ctermbg=NONE |\
#     hi VertSplit ctermbg=NONE |\
#     hi NonText ctermfg=0 |\
#     Codi $syntax" "$@"
# }

# Add and commit dotfiles
#dotc() {
#    #local files="$(dot status | awk -F': ' '/deleted|modified|new/ {print $2}' | fzf --multi --reverse --height=90% --prompt="Add files: " | sed 's/ /\ /g' | tr '\n' ' ')"
#    # local out_of_date=$(dot remote show origin | rg 'out of date')
#    # [ -z "$out_of_date" ] || { printf "Local is out of date, pulling first\n"; dot pull; }

#    local gitcmd='git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME'
#    local files
#    local again='y'

#    while [ "$again" = 'y' ]
#    do
#	files="$(git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME status -s | \
#	    fzf --ansi --multi --reverse --prompt="Add files: " --preview "git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME diff --color=always -- {+2}" | \
#	    awk '{for (i=2; i<=NF; i++) print $i}' | tr '\n' ' ')"

#	[ -z "$files" ] && return 0
#	$gitcmd add $files
#	$gitcmd commit
	
#	read -p $'\nCommit again? [y / anything]: ' again
#    done

#    echo -e '\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'
#    read -p $'\nPush? [y / anything]: ' confirm_push
#    [ "$confirm_push" = 'y' ] && dot push || echo "Didn't push"
#}

# Quickly switch WiFi network
chnet() {
    con-pagla() {
	echo "Scanning networks"
	nmcli d wifi rescan
	sleep 2

	echo "Connecting to Pagla Dashu"
	nmcli d wifi connect "Pagla Dashu"
    }
    con-antor() {
	echo "Scanning networks"
	nmcli d wifi rescan
	sleep 2

	echo "Connecting to ANTOR"
	nmcli d wifi connect "ANTOR"
    }
    if [ $# -ne 1 ]
    then
	echo 'Pass p/a/s as argument'
    else
	case $1 in
	    'p') con-pagla ;;
	    'a') con-antor ;;
	    's') nmcli connection show ;;
	    *) echo 'Pass p/a/s as argument' ;;
	esac
    fi
}

# Git Init
gi() {
    [ $# -ne 1 ] && { echo '1 arg: me/nsu.' ; return 0 ; }

    [ "$1" = "me" ] && mail='asad.noor.antor@gmail.com' || mail='asaduzzaman.noor@northsouth.edu'
    git init --initial-branch=master
    printf "[user]\n\tname = Asaduzzaman Noor\n\temail = %s\n[alias]\n\tc = commit\n\tco = checkout\n\tbr = branch" "$mail" >> .git/config
    printf ".gitignore" > .gitignore
    nvim README.adoc
    git add .
    git commit -m 'Initial commit'
}

# Clone git repository and cd into it
clone() {
    git clone "$1"
    repo_name=`echo "$1" | perl -nle 'm/([^\/]+(?=\.git))/; print $1'`
    cd $repo_name
}

# Extract files with 7z
reap() {
    local filen="$1"
    local foldern="${filen%.*}"
    7z x -o"$foldern" "$filen"
    cd "$foldern"
}

# }}}
# Keybindings {{{
# look into blerc for more bindings
# `bind` and `bind -x` <- look them up
#bind -x '"\C-o":"source $HOME/.bashrc\n"' # <- Can't run my functions due to blesh

# Reload bash config
bind '"\C-\M-r":"source $HOME/.bashrc\n"'
# }}}
# External sources {{{
# Completion
. /usr/share/bash-completion/bash_completion

# Install `pkgfile` to get this working
source /usr/share/doc/pkgfile/command-not-found.bash

# Broot
#source /home/asad/.config/broot/launcher/bash/br

# External `up` script for CDing
#. $HOME/.local/scripts/up.sh

# AutoCompletion for `bd` <- bd executable script is in local/bin directory
#. $HOME/.local/scripts/bd-completion.sh

# Enhanced CD -- z
eval "$(lua /usr/share/z.lua/z.lua --init bash enhanced once)"

# Source NIX package manager
#source /etc/profile.d/nix{,-daemon}.sh


# pyenv
#[ $TERM = "linux" ] && source ~/.profile
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

source /usr/share/nvm/init-nvm.sh

#source /home/asad/builds/sh-manpage-completions/completions/bash/_tar
# ExtraTerm extra commands
#. /media/data-drive/Downloads/ExtraTerm/extraterm-commands-0.58.0/setup_extraterm_bash.sh
# }}}
#export GOPATH="/home/asad/go:/home/asad/go-projects"
remove_duplicate_history
# ((_ble_bash)) && ble-attach
# vim: foldmethod=marker foldlevel=0

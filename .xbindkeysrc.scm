;; bind shift + vertical scroll to horizontal scroll events
(xbindkey '(shift "b:4") "xte 'mouseclick 6'")
(xbindkey '(shift "b:5") "xte 'mouseclick 7'")
;; Menu as middle mouse button
(xbindkey '(release "c:135") "xte 'mouseup 2'")
(xbindkey "c:135" "xte 'mousedown 2'")
;; (xbindkey '(release shift "b:3") "xte 'mouseup 2'")
;; (xbindkey '(shift "b:3") "xte 'mousedown 2'")

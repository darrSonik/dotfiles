" Custom Functions {{{
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    "Mark destination
    let curNum = winnr()
    let curBuf = bufnr( "%" )
    exe g:markedWinNum . "wincmd w"
    "Switch to source and shuffle dest->source
    let markedBuf = bufnr( "%" )
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' curBuf
    "Switch to dest and shuffle source->dest
    exe curNum . "wincmd w"
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf 
endfunction
" }}}
" Custom Defaults {{{
set title
set nocompatible
set shell=bash
filetype plugin indent on
syntax on
set termguicolors
colorscheme slate
set number relativenumber

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

set autoread
"set autowrite
"set autowriteall

"set ruler
set modeline
set modelines=5

" Copy to system clipboard
set clipboard=unnamedplus

" Indents - Tabs and Spaces
set tabstop=8
set shiftwidth=4
set softtabstop=4
" set expandtab
set smarttab

"set nowrap sidescroll=1

set mouse=a
set mousefocus
set mousehide
"set mousemodel=popup

set ignorecase
set smartcase
set hlsearch
set incsearch
autocmd InsertEnter * :setlocal nohlsearch
autocmd InsertLeave * :setlocal hlsearch

highlight Pmenu guibg=#5e644f
highlight PmenuSel guifg=Black

highlight Comment cterm=italic gui=italic

set spell spelllang=en_gb

augroup fixlatex
    autocmd!
    autocmd BufRead,BufEnter *.tex let g:tex_flavor='latex'
    autocmd BufRead,BufEnter *.tex setlocal conceallevel=2
augroup END

set foldmethod=syntax
" set nofoldenable
set foldlevel=20
" }}}
" Custom Keybindings {{{
let mapleader=' '
let maplocalleader='\'

nnoremap <TAB> :bn!<CR>
nnoremap <S-TAB> :bp!<CR>
nnoremap <M-TAB> gt

"au TermOpen * tnoremap <Esc> <c-\><c-n>

" Splits - Swtiching
map <M-h> <C-w>h
map <M-j> <C-w>j
map <M-k> <C-w>k
map <M-l> <C-w>l
" Splits - Swapping
nnoremap Zm :call MarkWindowSwap()<CR>
nnoremap Zs :call DoWindowSwap()<CR>
" Splits - Resizing
nmap <M-Up> :res +5<CR>
nmap <M-Down> :res -5<CR>
nmap <M-Right> :vert res +5<CR>
nmap <M-Left> :vert res -5<CR>
" Split Zoom
noremap Z- <c-w>_ \| <c-w>\|
noremap Z= <c-w>=

" Fix Spelling Mistakes on the fly
inoremap <M-\> <c-g>u<Esc>[s1z=`]a<c-g>u

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Remove trailing spaces
nmap <silent> <C-M-r> :%s/\s\+$//e<CR>
command! FixWhitespace :%s/\s\+$//e

" Open VIMRC
command! VC execute ":edit $MYGVIMRC"

" Reload Vim Config
command! RC execute "source $MYGVIMRC"
" }}}

" vim: fdm=marker
" vim: fdl=0

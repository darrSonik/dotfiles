#!/usr/bin/env bash
write_to_queue() {
    echo "$(xclip -selection clipboard -o)" >> /media/data-drive/.cache/aria2.session
}

start_download() {
    if [ -s '/media/data-drive/.cache/aria2.session' ]
    then
	terminator -T "Aria2" -e 'aria2c' &
    else
	dunstify -r 12 -u normal 'Aria2' 'Nothing to Download' 
    fi
}

gui() {
    prompt=$(echo -e "Toggle-View|Add-to-Queue|Download-Link|Start-Queue" | rofi -dmenu -i -sep "|" -p "Aria2" -no-custom)
    case $prompt in
        "Download-Link") write_to_queue && start_download ;;
        "Add-to-Queue")  write_to_queue ;;
        "Start-Queue")   start_download ;;
        "Toggle-View")   /home/asad/.local/scripts/toggle-visibility.sh 'Aria2-Downloader' ;;
        *) ;;
    esac
}

no_gui() {
    echo "$1" >> /media/data-drive/.cache/aria2.session
    start_download
}

[ $# -eq 1 ] && no_gui "$1" || gui

#!/usr/bin/env bash

toggle-rtorrent() {
    [ -z "$(xdotool search --name RTorrent)" ] && gtk-launch rtorrent || $HOME/.local/scripts/toggle-visibility.sh RTorrent
}

toggle-youtube-music() {
    if [ -z "$(xdotool search --class Youtube-YTP)" ]
    then
	termite --class='Youtube-YTP' --name='youtube-ytp-sh' --title='Youtube Music' --exec='/home/asad/.local/scripts/ytp'
    else
	/home/asad/.local/scripts/toggle-visibility.sh 'Youtube-YTP'
    fi
}

mpv-dialogue() {
    lnks="$(yad --width=400 --title="MPV" --form --field="Video:" --field="Subtitle:" --separator=" " )"

    mpv --fs --sub-file="$(echo $lnks | awk -F' ' '{print $2}')" "$(echo $lnks | awk -F' ' '{print $1}')"
}

task-manager() {
    if [ -z "$(xdotool search --name BpyTOP)" ]
    then
	termite --exec=bpytop
    else
	/home/asad/.local/scripts/toggle-visibility.sh BpyTOP
    fi
}

prompt=$(echo -e "rTorrent|YouTube Music|MPV Prompt|Task Manager" | rofi -dmenu -i -sep "|" -p "Run or Hide/Unhide" -no-custom)
case $prompt in
    "rTorrent"     ) toggle-rtorrent      ;;
    "YouTube Music") toggle-youtube-music ;;
    "Task Manager" ) task-manager         ;;
    "MPV Prompt"   ) mpv-dialogue         ;;
    *) ;;
esac

#!/usr/bin/env python
import rofi_menu
import sqlite3
#import subprocess

def get_bmarks(db_file: str) -> list[tuple]:
    con = sqlite3.connect(db_file)
    con.row_factory = sqlite3.Row
    cur = con.cursor()
    cur.execute('select * from webmarks')
    bmarks = cur.fetchall()
    con.close()
    return bmarks

class DeleteBookmark(rofi_menu.Menu):
    prompt = 'Delete:'
    bmarks = get_bmarks('/home/asad/.local/share/webmarks/database.db')
    items = [ rofi_menu.ShellItem(bmark[0], f'python /home/asad/.local/share/webmarks/delete-bmark.py "{bmark[0]}"') for bmark in bmarks ]

class Actions(rofi_menu.Menu):
    prompt = 'Actions: '
    items = [
        rofi_menu.ShellItem('Add Bookmark', 'python /home/asad/.local/share/webmarks/add-bmark.py'),
        rofi_menu.NestedMenu('Delete Bookmark', DeleteBookmark()),
        rofi_menu.BackItem('←')
    ]

class Bookmarks(rofi_menu.Menu):
    prompt = 'Bookmarks'
    bmarks = get_bmarks('/home/asad/.local/share/webmarks/database.db')
    items = [ rofi_menu.NestedMenu('Actions →\0icon\x1fgtk-edit', Actions()) ]
    for bmark in bmarks:
        items.append(rofi_menu.ShellItem(f'{bmark[1]}<span color="#adff2f" style="italic" size="x-small"> - ({bmark[0]})</span>', f'$BROWSER {bmark[0]}'))


if __name__ == "__main__":
    rofi_menu.run(Bookmarks())


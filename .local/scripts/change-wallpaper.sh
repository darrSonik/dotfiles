#!/usr/bin/env bash

(( $# > 1 )) && exit 0

mon1="/media/data-drive/Pictures/Backgrounds/f-1920x1080"
mon2="/media/data-drive/Pictures/Backgrounds/f-1280x1024"

search=${1:-dark}

rm "$mon1" "$mon2"
axel -q -n 1 -o "$mon1" "https://source.unsplash.com/1920x1080/?$search"
axel -q -n 1 -o "$mon2" "https://source.unsplash.com/1280x1024/?$search"
feh --bg-fill "$mon1" "$mon2"

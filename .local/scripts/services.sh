#!/bin/sh

carry_out() {
    # local pass=$(yad --title='Password' --text='Enter password for <b>asad</b>:' --image='dialog-password' --entry --hide-text)
    # [ -z "$pass" ] && {  dunstify -r 737 'Services' 'Not aunthenticated'; exit 0; }
    # echo $pass | sudo -S systemctl $1 $2
    sudo -A systemctl $1 $2
    dunstify -r 737 'Services' "<span foreground='#0f0' style='italic'>$1</span>: $2"
}

service_name=$(systemctl list-units --all --type=service --no-pager --no-legend | \
    tr -d '●' | \
    sed -e 's/inactive/○/' -e 's/active/●/' -e 's/loaded/✔/' -e 's/not-found/✘/' | \
    awk '{printf "%s %s <span foreground=\"#848482\" size=\"x-small\">%s</span>\n", $3, $1, $2}' | \
    #awk '{ printf "<span foreground=\"#98817b\">[%-8s]</span> %s <span foreground=\"#848482\" size=\"small\" style=\"italic\">(%s)</span>\n", $state, $1, $2}' | \
    rofi -dmenu -i -no-custom -p 'Services' -mesg 'Start/stop/restart a service' -markup-rows -window-title 'Managing Services' | \
    awk '{print $2}')


[ -z "$service_name" ] && exit

yad --text="<span size='x-large' weight='bold' foreground='green'>$service_name</span>" --text-align=center \
    --title="Select Action" --buttons-layout=center \
    --button=' Start!media-playback-start':1 \
    --button=' Stop!media-playback-stop':2 \
    --button=' Restart!media-playlist-repeat':3

case $? in
    1) carry_out start $service_name ;;
    2) carry_out stop $service_name ;;
    3) carry_out restart $service_name ;;
    *) dunstify -r 737 'Services' 'Action Aborted' ;;
esac

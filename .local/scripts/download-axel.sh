#!/usr/bin/env bash

open_in_pcmanfm() {
    pcmanfm /media/data-drive/Downloads
}

file_open() {
    mimeo $filename
}


cd /media/data-drive/Downloads

link="$1"

#tym --class=Downloader --name=AxelDownloader --title=Axel -e "echo -e '\n\e[5m\e[103m\e[30mBlinking With Background\e[0m' && axel $1"
tym --class=Downloader --name=AxelDownloader --title=Axel -e "axel $link"

filename="$(ls -t | head -1)"
fullname="/media/data-drive/Downloads/$filename"

action=$(dunstify -h string:x-dunst-stack-tag:axel -A 'open_fm,FileManger' -A 'open_file,Open' 'Axel' "Download Finished - $filename")

case "$action" in
    "open_fm"  ) pcmanfm /media/data-drive/Downloads ;;
    "open_file") mimeo $fullname ;;
    *) ;;
esac

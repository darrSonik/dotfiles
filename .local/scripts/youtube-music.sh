#!/usr/bin/env bash
# cmd='/home/asad/.local/scripts/toggle-visibility.sh Youtube-YTP'
# img='/media/data-drive/Music/music.png'
# q_cmd='quit && xdo close -N Youtube-YTP'
if [ -z "$(xdotool search --class Youtube-YTP)" ]
then
    #yad --notification --image=$img --command="$cmd" --menu=quit!$q_cmd &
    termite --class='Youtube-YTP' --name='youtube-ytp-sh' --title='Youtube Music' --exec='ytp'
else
    /home/asad/.local/scripts/toggle-visibility.sh 'Youtube-YTP'
fi


#!/usr/bin/env bash
sess=($(tmux ls 2> /dev/null | cut -d: -f1))
case "${#sess[@]}" in
    "0") tmux new-session -s Terminal ;;
    "1") tmux attach -t "${sess[0]}" ;;
      *) select_sess=$(echo -e "${sess[@]}" | rofi -dmenu -i -sep ' ' -p 'Select Session' -no-custom)
         [ -z "$select_sess" ] && xdo close -N Terminal || tmux attach -t "$select_sess" ;;
esac

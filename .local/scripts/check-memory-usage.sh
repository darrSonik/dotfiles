#!/usr/bin/env bash
_usage() {
    printf "Enter one argument, program name\n"
    exit 0
}
if [ $# -eq 1 ]
then
    smem | rg -i "$1" | awk '{Total+=$6} END {print Total/1024" MB"}' || _usage
else
    _usage
fi

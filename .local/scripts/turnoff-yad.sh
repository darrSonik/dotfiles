#!/bin/sh

yad --icon-size=400 --timeout=5 --timeout-indicator=top --title="Turn Off??" --buttons-layout=center \
    --button=' Shut Down!system-shutdown':1 \
    --button=' Restart!system-reboot':2 \
    --button=' Sleep!system-suspend':3
foo=$?
case $foo in
    1) systemctl poweroff ;;
    2) systemctl reboot ;;
    3) systemctl suspend ;;
    *) echo 'Cancelled' ;;
esac

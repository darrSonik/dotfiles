#!/usr/bin/env bash

# getpass() {
#     pass=$(yad --title='Password' --text='Enter password for <b>asad</b>:' --image='dialog-password' --entry --hide-text)
#     [ -z "$pass" ] && { dunstify -r 863 'Alt-F2' 'Command Aborted'; exit 0; }
# }
hist='/media/data-drive/.cache/alt-f2-hist'

# cmd_list=$(printf "%s\n%s\n%s\n%s\n%s" \
#                   "$(compgen -c)" \
#                   "$(ls $HOME/.local/share/applications)" \
#                   "$(ls /usr/share/applications)" \
#                   "$(ls $HOME/Applications | xargs -I % echo $HOME/Applications/%)" \
#                   "$(cat $d/.cache/alt-f2-hist)" | \
#                sort -u | tr '\n' '!')

cmd_list=$(printf "%s\n%s\n%s\n%s\n%s" \
                  "$(compgen -c)" \
                  "$(ls $HOME/.local/share/applications)" \
                  "$(ls /usr/share/applications)" \
                  "$(cat $d/.cache/alt-f2-hist)" | \
               sort -u | tr '\n' '!')

prompt=$(yad --width=400 --title='Run Prompt' --window-icon=distributor-logo-archlinux --form --separator='-----' --field='Execute:CE' --complete=all "$cmd_list" --field='Run in Terminal':CHK)
cmd=$(echo $prompt | awk -F'-----' '{ print $1 }')
termn=$(echo $prompt | awk -F'-----' '{ print $2 }')
[ -z "$cmd" ] && exit 0

if [[ $cmd = *" "* ]]
then
    echo "$cmd" >> $hist
    awk '!x[$0]++' $hist >| /tmp/alt-f2
    mv /tmp/alt-f2 $hist
fi

# [ $termn = "TRUE" ] && $TERMINAL -e "$cmd" || bash -c "$cmd"

if [[ $cmd == *.desktop* ]]
then
    gtk-launch $cmd
else
    if [[ $termn = "TRUE" ]]
    then
       $TERMINAL -e "$cmd"
    else
      sh -c "$cmd"
      [ $? -ne 0 ] && dunstify -u critical "Error" "Couldn't execute \`${cmd}\`"
    fi
fi

#if [ $termn = "TRUE" ]
#then
#    terminator -e "$cmd"
#else
#    [ "sudo" = ${cmd%% *} ] && bash -c "$cmd $pass" || bash -c "$cmd"
#fi

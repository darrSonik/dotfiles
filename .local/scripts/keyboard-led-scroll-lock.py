#!/usr/bin/env python3

import subprocess as s

xset_out       = s.Popen(('xset', 'q'), stdout = s.PIPE)
xset_filter    = s.check_output(('awk', '/LED/ {print $NF}'), stdin = xset_out.stdout, encoding='UTF-8')
scroll_lock_on = int(xset_filter) // 4 == 1

if scroll_lock_on:
    s.run(('xset', '-led', 'named', 'Scroll Lock'))
else:
    s.run(('xset', 'led', 'named', 'Scroll Lock'))

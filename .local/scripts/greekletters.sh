#/usr/bin/env bash

# Letters {{{
letters="Alpha: Α α\
    |Beta: Β β\
    |Gamma: Γ γ\
    |Delta: Δ δ\
    |Epsilon: Ε ε\
    |Zeta: Ζ ζ\
    |Eta: Η η\
    |Theta: Θ θ\
    |Iota: Ι ι\
    |Kappa: Κ κ\
    |Lambda: Λ λ\
    |Mu: Μ μ\
    |Nu: Ν ν\
    |Xi: Ξ ξ\
    |Omicron: Ο ο\
    |Pi: Π π\
    |Rho: Ρ ρ\
    |Sigma: Σ σ\
    |Tau: Τ τ\
    |Upsilon: Υ υ\
    |Phi: Φ φ\
    |Chi: Χ χ\
    |Psi: Ψ ψ\
    |Omega: Ω ω"
# }}}

selected_letter=$(echo $letters | rofi -dmenu -i -sep '|')

if [ -z "$selected_letter" ]
then
    exit
else
    letter_type=$(echo 'Capital Small' | rofi -dmenu -sep ' ')
    case $letter_type in
	'Capital') echo $selected_letter | cut -d' ' -f2 | tr -d '\n' | xclip -sel clip ;;
	'Small') echo $selected_letter | cut -d' ' -f3 | tr -d '\n' | xclip -sel clip ;;
	*) ;;
    esac
fi

# vim: fdm=marker fdl=0

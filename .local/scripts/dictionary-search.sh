#!/usr/bin/env bash

#yad --entry --entry-label="Word to search: " --entry-text="$(xclip -sel clip -o)"
#yad --text-align=center --text="Search Dictionary" --entry --entry-text="$(xclip -sel clip -o)"

pri="$(xclip -selection primary -o)"
txt="${pri:-}"
word=$(yad --text-align=center --text="Search Dictionary" --entry --entry-text="$txt")

[[ -z $word ]] || dict "${word}" | yad --title="dict" --width=800 --height=600 --text-info --no-buttons

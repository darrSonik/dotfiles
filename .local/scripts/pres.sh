#!/bin/sh
#read -p 'Enter filename: ' filename

create_presentation() {
    cp ~/.config/inkscape/templates/for-sozi.svg "$1"
    nohup inkscape "$1" > /dev/null 2>&1 &

}

conflict() {
    yad --text="File exists." --button=' Auto-Rename!view-task-add':1 --button=' Overwrite!edit-delete':2 --button=' Cancel!gtk-cancel':3
    local status=$?

    case $status in
	1) create_presentation "$filename-1.svg" ;;
	2) create_presentation "$filename.svg" ;;
	*) exit 0 ;;
    esac
}

name="$(yad --entry --entry-label='Name:')"
[ -z "$name" ] && exit 0
filename="/media/data-drive/Pictures/$name"
[ -f "$filename.svg" ] && conflict || create_presentation "$filename.svg"

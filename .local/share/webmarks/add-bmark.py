import sqlite3
import subprocess

def add_bmark(db_file: str) -> None:
    cmd = ['yad', '--width=400', '--title=Add Bookmark', '--text=Add Bookmark', '--form', '--field=URL', '--field=Name']
    vals = subprocess.check_output(cmd).decode('UTF-8').split('|')

    con = sqlite3.connect(db_file)
    cur = con.cursor()
    cur.execute('insert into webmarks values (?, ?)', vals[:2])
    con.commit()
    con.close()

if __name__ == '__main__':
    add_bmark('/home/asad/.local/share/webmarks/database.db')

#!/usr/bin/env python

import sqlite3
import sys

def delete_bmark(db_file: str, bmark_url: str) -> None:
    con = sqlite3.connect(db_file)
    cur = con.cursor()
    prep = (bmark_url,)
    cur.execute('delete from webmarks where url=?', prep)
    con.commit()
    con.close()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        print(str(sys.argv[1]))
        delete_bmark('/home/asad/.local/share/webmarks/database.db', str(sys.argv[1]))
    else:
        print('One argument please')

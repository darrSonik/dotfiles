#!/usr/bin/env bash
#[ $# -ne 1 ] && exit 0
qmpv() {
    if [ -z "$(pidof mpv)" ]
    then
	[ -f "/tmp/mpvsocket" ] && rm /tmp/mpvsocket
	bspc rule -a mpv -o state=floating follow=on rectangle=1024x576+130+197 #focus=off
	mpv --no-terminal --input-ipc-server=/tmp/qutempv "$1"
    else
	echo loadfile "$1" append-play | socat - /tmp/qutempv
    fi
}

qllpp() {
    axel -o "/tmp/qutetemp.pdf" "$1"
    llpp "/tmp/qutetemp.pdf"
    rm -f "/tmp/qutetemp.pdf"
}

qdownvid() {
    cd "$d/Downloads"
    youtube-dl "$1"
    dunstify -r 983 'Youtube DL' "Download finished <$(ls -t | head -1)>"
}

qdown() {
    cd $d/Downloads
    axel "$1"
    dunstify -r 983 'Qute-Axel' "Download finished <$(ls -t | head -1)>"
}

cmd=$(yad --width=600 --title='Open with' --text='Command to run:' --image='distributor-logo-archlinux' --licon='input-keyboard' --entry --completion --complete=regex --rest=/media/data-drive/.cache/qute-open.txt)
[ -z "$cmd" ] && exit 0

"$cmd" "$1"

echo "$cmd" >> /media/data-drive/.cache/qute-open.txt
awk '!x[$0]++' /media/data-drive/.cache/qute-open.txt >| /tmp/quteopen
mv /tmp/quteopen /media/data-drive/.cache/qute-open.txt

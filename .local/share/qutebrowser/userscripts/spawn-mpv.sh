#!/usr/bin/env bash

if [ -z "$(pidof mpv)" ]
then
    #mkfifo /tmp/mpvsocket
    #nc -lU /tmp/mpvsocket &
    [ -f "/tmp/mpvsocket" ] && rm /tmp/mpvsocket
    bspc rule -a mpv -o state=floating follow=on rectangle=1024x576+130+197 #focus=off
    mpv --no-terminal --input-ipc-server=/tmp/qutempv "$1"
else
    echo loadfile "$1" append-play | socat - /tmp/qutempv
fi

#!/usr/bin/env bash
#[ $# -ne 1 ] && exit 0
ompv() {
    if [ -z "$(pidof mpv)" ]
    then
	[ -f "/tmp/mpvsocket" ] && rm /tmp/mpvsocket
	bspc rule -a mpv -o state=floating follow=on rectangle=1024x576+130+197 #focus=off
	mpv --no-terminal --input-ipc-server=/tmp/mpvsocket "$1"
    else
	echo loadfile "$1" append-play | socat - /tmp/mpvsocket
    fi
}

pdfl() {
    axel -o "/tmp/temp-pdf-script.pdf" "$1"
    llpp "/tmp/temp-pdf-script.pdf"
    rm -f "/tmp/temp-pdf-script.pdf"
}

ytdl() {
    cd "$d/Downloads"
    youtube-dl "$1"
    dunstify -r 983 'Youtube DL' "Download finished <$(ls -t | head -1)>"
}

ytvlc() {
    vlc --one-instance --playlist-enqueue "$1"
}

axld() {
    cd $d/Downloads
    axel "$1"
    dunstify -r 983 'Qute-Axel' "Download finished <$(ls -t | head -1)>"
}

cmd=$(yad --width=600 --title='Open with' --text='Command to run:' --image='distributor-logo-archlinux' --licon='input-keyboard' --entry --completion --complete=regex --rest=/media/data-drive/.cache/chrome-external.txt)
[ -z "$cmd" ] && exit 0

"$cmd" "$1"

echo "$cmd" >> /media/data-drive/.cache/chrome-external.txt
awk '!x[$0]++' /media/data-drive/.cache/chrome-external.txt >| /tmp/chrome-external-app
mv /tmp/chrome-external-app /media/data-drive/.cache/chrome-external.txt

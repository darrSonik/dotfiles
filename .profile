export XDG_CACHE_HOME="/media/data-drive/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

export LUA_PATH='/usr/share/lua/5.4/?.lua;/usr/share/lua/5.4/?/init.lua;/usr/lib/lua/5.4/?.lua;/usr/lib/lua/5.4/?/init.lua;./?.lua;./?/init.lua;/home/asad/.luarocks/share/lua/5.4/?.lua;/home/asad/.luarocks/share/lua/5.4/?/init.lua'
export LUA_CPATH='/usr/lib/lua/5.4/?.so;/usr/lib/lua/5.4/loadall.so;./?.so;/home/asad/.luarocks/lib/lua/5.4/?.so'
export PYENV_ROOT="$HOME/.pyenv"
export NPM_PACKAGES="${HOME}/.npm-packages"
export NODE_PATH="$NPM_PACKAGES/lib/node_modules:$NODE_PATH"
export GOPATH="$HOME/go:$HOME/go-projects"
export PATH=$PATH:$HOME/.local/bin:$HOME/.local/scripts:$HOME/.yarn/bin:$HOME/.luarocks/bin:$PYENV_ROOT/bin:$HOME/flutter/bin:$NPM_PACKAGES/bin:$XDG_CONFIG_HOME/emacs/bin

export _ZL_DATA="~/.local/share/zluahistory"
#export _ZL_ADD_ONCE='1'

export EDITOR=nvim
export VISUAL=nvim
#export VISUAL='gvim -f'
export ABDUCO_CMD="bash"
export d="/media/data-drive"

#eval $(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
#export SSH_AUTH_SOCK

#eval "$(pyenv init --path)"
#export PYENV_VIRTUALENV_DISABLE_PROMPT=0

MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

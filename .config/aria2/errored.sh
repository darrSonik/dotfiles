#!/usr/bin/env bash
#notify-send -c transfer.error -u critical "Aria2" "Problem Downloading File - $(ls -1 -t ~/Downloads | head -1)"
dunstify -r 12 -u critical "Aria2" "Problem Downloading File - $(ls -1 -t /media/data-drive/Downloads | head -1)"


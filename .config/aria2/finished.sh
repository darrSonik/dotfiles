#!/usr/bin/env bash
#notify-send -c transfer.complete -u normal "Aria2" "Download Finished - $(ls -1 -t ~/Downloads | head -1)"
dunstify -r 12 -u normal "Aria2" "Download Finished - $(ls -1 -t /media/data-drive/Downloads | head -1)"

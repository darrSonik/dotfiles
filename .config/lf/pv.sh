#!/bin/bash
case $(file --mime-type -b "$1") in
    text/*) bat "$1" ;;
    application/json) bat "$1" ;;
    application/zip) 7z l "$1" ;;
    *) mediainfo "$1";;
esac

#case $(file --mime-type -b "$1") in
#    text/*) ccat --color=always "$1" ;;
#    application/json) ccat "$1" ;;
#    *) mediainfo "$1";;
#esac

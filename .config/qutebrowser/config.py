config.load_autoconfig(True)
c.qt.args=["ignore-gpu-blocklist", "enable-gpu-rasterization", "enable-accelerated-video-decode", "enable-native-gpu-memory-buffers", "num-raster-threads=4"]

c.aliases = {"q": "close", "qa": "quit", "w": "session-save", "wq": "quit --save", "wqa": "quit --save", "r": "restart"}
c.url.searchengines = {"d": "https://duckduckgo.com/?q={}", "DEFAULT": "https://search.brave.com/search?q={}", "g":"https://www.google.com/search?q={}"}

c.content.blocking.enabled = True
c.content.blocking.method = 'both'
c.content.blocking.adblock.lists = ['https://easylist.to/easylist/easylist.txt', 'https://easylist.to/easylist/easyprivacy.txt', 'https://easylist-downloads.adblockplus.org/easylistdutch.txt', 'https://easylist-downloads.adblockplus.org/abp-filters-anti-cv.txt', 'https://www.i-dont-care-about-cookies.eu/abp/', 'https://secure.fanboy.co.nz/fanboy-cookiemonster.txt']

c.content.default_encoding = 'utf-8'
c.content.pdfjs = True

c.window.hide_decoration = True
c.tabs.show = 'switching'
c.statusbar.show = 'in-mode'
c.window.title_format = '{private}{current_title}'
c.completion.open_categories = ['quickmarks', 'bookmarks', 'history']

#c.editor.command = ["code", "-w", "{file}"]
#c.spellcheck.languages = ["en-GB"]
#c.colors.webpage.bg = ''
c.colors.webpage.preferred_color_scheme = 'dark'
c.colors.webpage.darkmode.policy.page = 'smart'
c.colors.webpage.darkmode.policy.images = 'never'
c.scrolling.smooth = True
#c.tabs.select_on_remove = 'last-used'
c.tabs.mode_on_change = 'persist'
#c.content.notifications.presenter = 'qt'
c.content.notifications.presenter = 'libnotify'

c.url.start_pages = ['~/.config/qutebrowser/startpage/index.html']
c.url.default_page = '~/.config/qutebrowser/startpage/index.html'

c.downloads.location.directory = '/media/data-drive/Downloads'
c.fileselect.folder.command = ['yad', '--file', '--directory']
c.fileselect.single_file.command = ['yad', '--file']
c.fileselect.multiple_files.command = ['yad', '--file', '--multiple']

c.hints.selectors["code"] = [
    # Selects all code tags whose direct parent is not a pre tag
    ":not(pre) > code",
    "pre"
]
config.bind(';c', 'hint code userscript code_select.py', mode='normal')

config.unbind('J', mode='normal')
config.bind('J', 'tab-prev', mode='normal')
config.unbind('K', mode='normal')
config.bind('K', 'tab-next', mode='normal')

config.bind('gH', 'open {url:host}')

config.unbind('gJ', mode='normal')
config.bind('gh', 'tab-move -', mode='normal')
config.unbind('gK', mode='normal')
config.bind('gl', 'tab-move +', mode='normal')
config.bind('\\', 'set-cmd-text -s :tab-select')

config.bind('cn', 'tab-only -p', mode='normal')
config.bind('cp', 'tab-only -n', mode='normal')
config.bind('cm', 'set-cmd-text -s :quickmark-del', mode='normal')
config.bind('cM', 'set-cmd-text -s :bookmark-del', mode='normal')

config.bind('<Ctrl-]>', 'set-cmd-text -s :fake-key', mode='normal')
config.bind('<Ctrl-[>', ':fake-key <Esc>', mode='normal')

config.bind(';p', 'hint links run open -p {hint-url}')
config.bind(';w', 'hint links run open -w {hint-url}')
config.bind('Wo', 'set-cmd-text -s :open -p')
config.bind('WO', 'set-cmd-text -s :open -p {url:pretty}')
config.bind('C', 'config-edit')
config.bind('e', 'set-cmd-text -s :tab-take')
config.bind('E', 'set-cmd-text -s :tab-give')

config.bind(',v', 'hint links spawn --userscript spawn-mpv.sh {hint-url}')
config.bind(',d', 'hint -m number links spawn --userscript download-aria2.sh {hint-url}')
config.bind(',o', 'hint -m number links spawn --userscript open-with.sh {hint-url}')
#config.bind(',b', 'spawn --userscript qute-bitwarden')
#config.bind(',p', 'hint links spawn --userscript pdf-open.sh {hint-url}')

config.bind('zd', 'config-cycle colors.webpage.darkmode.enabled True False', mode='normal')
config.bind('zs', 'config-cycle -t statusbar.show always in-mode', mode="normal")
config.bind('zt', 'config-cycle -t tabs.show always switching', mode="normal")
config.bind('zz', 'tab-focus last', mode='normal')
config.bind('Zo', 'spawn --userscript open-localhost.sh', mode='normal')

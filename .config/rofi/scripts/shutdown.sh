#!/bin/sh

confirm() {
    yad --text-align=center --text "<span size='large' weight='bold'>$1</span>" --button=' !dialog-ok':0 --button=' !dialog-cancel':1
}

off=''
resp=''
nap=''

txt="<span style='italic' size='small'>System:   </span><span foreground='#4ee44e' weight='bold'>$(uptime -p)</span>"

action="$(printf '%s;%s;%s' '' '' '' | rofi -dmenu -window-title 'Power Menu' -sep ';' -mesg "$txt" -theme shutdown.rasi)"

case $action in
    $off )
	confirm 'Turn off ?'
	[ $? -eq 0 ] && systemctl poweroff
	;;
    $resp)
	confirm 'Reboot ?'
	[ $? -eq 0 ] && systemctl reboot
	;;
    $nap ) systemctl suspend ;;
    *    ) ;;
esac

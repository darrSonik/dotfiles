function job-control -d "Control jobs"
    if test 1 -ne (count $argv)
	echo 'What to do? Kill, Disown, Send to foreground or background?'
    else
	switch $argv[1]
	    case 'fg'
		set jobn (jobs | fzf --reverse --height=20% --prompt='Bring to foreground: ' | cut -f1)
		[ -z $jobn ] || fg "%$jobn"
	    case 'bg'
		set jobn (jobs | fzf --reverse --height=20% --prompt='Continue in background: ' | cut -f1)
		[ -z $jobn ] || bg "%$jobn"
	    case 'disown'
		set jobn (jobs | fzf --reverse --height=20% --prompt='Disown: ' | cut -f1)
		[ -z $jobn ] || disown "%$jobn"
	    case 'kill'
		set jobn (jobs | fzf --reverse --height=20% --prompt='Kill: ' | cut -f2)
		[ -z $jobn ] || kill "$jobn"
	    case "*"
		echo 'Pass 1 arg: kill/disown/fg/bg'
	end
    end
end

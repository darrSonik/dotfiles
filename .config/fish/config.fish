if status is-interactive
    # Settings {{{
    set fish_cursor_default block blink
    set fish_cursor_visual underscore blink
    set fish_cursor_replace_one underscore blink
    set fish_cursor_insert line blink
    fzf_configure_bindings --directory=\cf
    # }}}
    # Aliases and abbreviations {{{
    alias ls "ls --color=auto"
    alias grep "grep --colour"
    alias zf "z -I"
    alias zb "z -b"
    alias tree "tree -C"
    alias nsudoc "cp /media/data-drive/Documents/NSU/latex/* ./ && nvim document.tex"
    alias dotf "git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME"
    alias uni 'mimeo "/media/data-drive/Pictures/ScreenShots/Varsity Schedule.png"'
    alias vfc "vf $HOME/.config"
    alias vfs "vf $HOME/.local/scripts"
    alias jb 'job-control bg'
    alias jf 'job-control fg'
    alias jd 'job-control disown'
    alias jk 'job-control kill'

 
    abbr -a O mimeo
    abbr -a L lfcd
    abbr -a v nvim
    abbr -a D devour
    abbr -a P pacui
    abbr -a md mkdir -p
    abbr -a dr dragon-drop
    abbr -a dup rsync -r -u -P
    abbr -a reloc rsync -r -u --remove-source-files -P
    abbr -a fixk "setxkbmap -option caps:swapescape ; xset r rate 200 35"
    # }}}
    # Variables {{{
    set -gx LESS_TERMCAP_mb \e'[1;32m'
    set -gx LESS_TERMCAP_md \e'[1;32m'
    set -gx LESS_TERMCAP_me \e'[0m'
    set -gx LESS_TERMCAP_se \e'[0m'
    set -gx LESS_TERMCAP_so \e'[1;33m'
    set -gx LESS_TERMCAP_ue \e'[0m'
    set -gx LESS_TERMCAP_us \e'[1;4;31m'
    #set -Ux GOPATH /home/asad/go
    #set -Uax GOPATH /home/asad/go-projects
    # }}}
    # More {{{
    # pyenv
    source (pyenv init - | psub)
    source (pyenv virtualenv-init - | psub)
    # }}}
end
# vim: fdm=marker fdl=0

#!/bin/bash

# Terminate already running bar instances
#killall -q polybar
killall -9 polybar

# Wait until the processes have been shut down
#while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

# Launch Polybar, using default config location ~/.config/polybar/config
#polybar minimal &

# For multi-monitor
#for m in $(polybar --list-monitors | cut -d":" -f1); do
#    MONITOR=$m polybar --reload minimal &
#done

for m in $(polybar --list-monitors | cut -d":" -f1)
do
    case $m in
	"DP-2") MONITOR=$m polybar --reload minimal &> /dev/null & ;;
	"HDMI-0") MONITOR=$m polybar --reload nada-bar &> /dev/null & ;;
	*) ;;
    esac
done

#polybar --reload single &> /dev/null &
echo "Polybar launched..."

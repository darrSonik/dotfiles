#!/usr/bin/env bash

wins=($(bspc query -N -n .window))

for window in ${wins[@]}
do
    xprop -id $window WM_CLASS | awk -F'[=,]' 'gsub(/[" ]/, "") { print $3 }'
done

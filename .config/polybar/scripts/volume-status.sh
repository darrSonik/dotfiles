#!/usr/bin/env bash
#vol=$(pamixer --get-volume)
pamixer --get-mute && icon=🔇 || icon=🔉
printf "%s %d%%\n" $icon $(pamixer --get-volume)

#!/usr/bin/env python
import subprocess

get_stat = subprocess.run(['pamixer', '--get-mute'], capture_output=True, text=True)
vol_stat = '🔇' if get_stat.stdout.strip() == 'true' else '🔉'

vol = subprocess.run(['pamixer', '--get-volume'], capture_output=True, text=True)

print(f'{vol_stat} {vol.stdout.strip()}%', end='')


#!/bin/sh
ssid="$(nmcli -t -f name connection show --active | head -1)"
# pub_ip="$(curl -s https://ipecho.net/plain)"
pub_ip="$(curl checkip.amazonaws.com)"

printf "%s [%s]" "$ssid" "$pub_ip"

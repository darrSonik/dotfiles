#!/usr/bin/env bash

#netlist="nmcli -t -f name,device connection show --active"
#cons=$($netlist | wc -l)

wifi_check="$(nmcli -t -f name,device connection show --active | rg wlp4s0)"
[ -z $wifi_check ] && ssid='Not connected' || ssid="$(echo $wifi_check | cut -d: -f1)"

vpn_status=$(nmcli -t -f name,device connection show --active | rg wgcf)

# case "$cons" in
#     "2") vpn_status="on" ; ssid="$($netlist | sed -n '1 p')" ;;
#     "1") ssid="$($netlist)" ;;
#     *  ) ssid="Not connected" ;;
# esac

init() {
    if [ "$ssid" = "Not connected" ]
    then
	printf "%s" "$ssid"
    else
	pub_ip="$(curl checkip.amazonaws.com)"
	[ -z $vpn_status ] && ip_show="$pub_ip" || ip_show="$pub_ip []"
	printf "%s  %s" "$ssid" "$ip_show"
    fi
}

updatename() {
    #[ -z $vpn_status ] && echo "$ssid" || echo "$ssid []"
    polybar-msg hook wifivpnipc 1
}

vpn_toggle() {
    # case "$cons" in
	# "2") sudo -A wg-quick down wgcf-profile && dunstify -r 876 'VPN' '<span foreground="#f00">Disconnected</span>' ;;
	# "1") sudo -A wg-quick up wgcf-profile   && dunstify -r 876 'VPN' '<span foreground="#0f0">Connected</span>' ;;
	# *  ) ;;
    # esac
    if [ -z $vpn_status ]
    then
	sudo -A wg-quick up wgcf-profile
	dunstify -r 876 'VPN' '<span foreground="#0f0">Connected</span>'
    else
	sudo -A wg-quick down wgcf-profile
	dunstify -r 876 'VPN' '<span foreground="#f00">Disconnected</span>'
    fi
    sleep 1
    polybar-msg hook wifivpnipc 1
}

change_wifi() {
    dunstify -r 876 'WiFi' 'Changing connection'
    nmcli device wifi rescan
    sleep 2

    netcon="nmcli device wifi connect"

    if [ "$ssid" = "ANTOR" ]
    then
	$netcon 'Pagla Dashu'
	dunstify -r 876 'WiFi' 'Connected to <span foreground="#ff0">Pagla Dashu</span>'
    else
	$netcon 'ANTOR'
	dunstify -r 876 'WiFi' 'Connected to <span foreground="#ff0">ANTOR</span>'
    fi
    polybar-msg hook wifivpnipc 1
}

confirm() {
    yad --text-align=center --text "<span size='large' weight='bold'>$1</span>" --button=' !dialog-ok':0 --button=' !dialog-cancel':1
}

#pub_ip="$(curl -s https://ipecho.net/plain)"

case "$1" in
    'wifi') confirm 'Change WiFi ?' ; [ $? -eq 0 ] && change_wifi ;;
    'vpn' ) confirm 'Toggle VPN ?'  ; [ $? -eq 0 ] && vpn_toggle  ;;
    'name') updatename ;;
    'init') init ;;
	* ) ;;
esac

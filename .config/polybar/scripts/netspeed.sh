#!/usr/bin/env bash
DEVICE=$1
IS_GOOD=0
for GOOD_DEVICE in `grep ":" /proc/net/dev | awk '{print $1}' | sed s/:.*//`; do
        if [ "$DEVICE" = "$GOOD_DEVICE" ]; then
                IS_GOOD=1
                break
        fi
done
if [ $IS_GOOD -eq 0 ]; then
        echo "Device $DEVICE not found. Should be one of these:"
        grep ":" /proc/net/dev | awk '{print $1}' | sed s/:.*//
        exit 1
fi

###REAL STUFF
LINE=`grep $1 /proc/net/dev | sed s/.*://`;
RECEIVED1=`echo $LINE | awk '{print $1}'`
TRANSMITTED1=`echo $LINE | awk '{print $9}'`
TOTAL=$(($RECEIVED1+$TRANSMITTED1))

SLP=1
sleep $SLP

LINE=`grep $1 /proc/net/dev | sed s/.*://`;
RECEIVED2=`echo $LINE | awk '{print $1}'`
TRANSMITTED2=`echo $LINE | awk '{print $9}'`
SPEED=$((($RECEIVED2+$TRANSMITTED2-$TOTAL)/$SLP))
INSPEED=$((($RECEIVED2-$RECEIVED1)/$SLP))
OUTSPEED=$((($TRANSMITTED2-$TRANSMITTED1)/$SLP))

[ "$1" = "wlp4s0" ] && interface="" || interface=""

echo "$interface ▾$(($INSPEED/1024)) KiB/s  ▴$(($OUTSPEED/1024)) KiB/s";

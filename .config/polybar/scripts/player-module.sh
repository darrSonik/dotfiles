#!/usr/bin/env bash

stat="$(playerctl status 2> /dev/null)"
[ -z "$(xdotool search --class Youtube-YTP)" ] && _ytp='' || _ytp='YT '
case "$stat" in
    'Playing') printf "$_ytp ▶" ;;
    'Paused')  printf "$_ytp ▮▮" ;;
    *)         printf ' ' ;;
esac

#!/usr/bin/env bash

get_status() {
    wifi_check="$(nmcli -t -f name,device connection show --active | rg wlp4s0)"
    vpn_check=$(nmcli -t -f name,device connection show --active | rg wgcf)

    [[ -z $wifi_check ]] && ssid='None' || ssid="$(echo $wifi_check | cut -d: -f1)"
    [[ -z $vpn_check ]]  &&  vpn='off'  ||  vpn='on'
}

vpn_toggle() {
    get_status 
    if [ "$vpn" = "off" ]
    then
	sudo -A wg-quick up wgcf-profile
	dunstify -r 876 'VPN' '<span foreground="#0f0">Connected</span>'
    else
	sudo -A wg-quick down wgcf-profile
	dunstify -r 876 'VPN' '<span foreground="#f00">Disconnected</span>'
    fi
}

change_wifi() {
    yad --text-align=center --text "<span size='large' weight='bold'>Select Network</span>" --button='ANTOR':2 --button='Pagla Dashu':3
    case "$?" in
	"2") wifi_name='ANTOR' ;;
	"3") wifi_name='Pagla Dashu' ;;
	*  ) wifi_name=$ssid
    esac

    nmcli device wifi rescan
    sleep 2

    nmcli device wifi connect "$wifi_name"
    dunstify -r 876 'WiFi' "Connected to <span foreground='#ff0'>$wifi_name</span>"
}

confirm() {
    yad --text-align=center --text "<span size='large' weight='bold'>$1</span>" --button=' !dialog-ok':0 --button=' !dialog-cancel':1
}

print_info() {
    get_status
    if [ "$ssid" = "None" ]
    then
	printf "Not connected"
    else
	[ "$vpn" = "on" ] && printf "%s []" "$ssid" || printf "%s" "$ssid"
    fi
}

case "$1" in
    'wifi') confirm 'Change WiFi ?' ; [ $? -eq 0 ] && change_wifi ;;
    'vpn' ) confirm 'Toggle VPN ?'  ; [ $? -eq 0 ] && vpn_toggle  ;;
    'name') print_info ;;
	* ) ;;
esac

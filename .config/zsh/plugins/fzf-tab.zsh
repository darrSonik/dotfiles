# https://github.com/Aloxaf/fzf-tab
PLUGIN_DIR="$(dirname "$0")/fzf-tab"
source $PLUGIN_DIR/fzf-tab.zsh

zstyle ':completion:*:descriptions' format '[%d]'
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'exa -1 --color=always $realpath'
zstyle ':fzf-tab:*' continuous-trigger '/'

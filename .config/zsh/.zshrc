# Misc {{{
setopt autocd beep extendedglob notify
unsetopt nomatch
#setopt nomatch
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/asad/.config/zsh/.zshrc'
autoload -U colors && colors

setopt correct
SPROMPT='Correct %F{red}%R%f to %F{green}%r%f? [Yes, No, Abort, Edit]: '

autoload -Uz compinit
compinit
# End of lines added by compinstall
# }}} 
# History settings {{{
HISTFILE=~/.config/zsh/histfile
HISTSIZE=10000000
SAVEHIST=$HISTSIZE
setopt BANG_HIST               # Treat the '!' character specially during expansion
#setopt EXTENDED_HISTORY       # Write the history file in the ":start:elapsed;command" format
setopt INC_APPEND_HISTORY      # Write the history file immediately, not after exiting the shell
setopt SHARE_HISTORY           # Share history among sessions
setopt HIST_EXPIRE_DUPS_FIRST  # Expire dupicate endtries first while trimming the history
setopt HIST_IGNORE_DUPS        # Don't record duplicate entry
setopt HIST_IGNORE_ALL_DUPS    # Delete old duplicate records if found
setopt HIST_SAVE_NO_DUPS       # Don't write a duplicate entry
setopt HIST_FIND_NO_DUPS       # Don't display a line that's previously found
setopt HIST_IGNORE_SPACE       # Ignore commands than start with a space
setopt HIST_REDUCE_BLANKS      # Remove superflous blanks before recording history
setopt HIST_VERIFY             # Don't execute immediately upon history expansion
setopt HIST_BEEP               # Beep when accessing nonexistent history
# }}}
# Prompt {{{
function _exit_status() {
  local last_exit_status=$?

  export last_exit=""
  if [ -z "${last_exit_status}" ]; then
    last_exit=""
  elif (( last_exit_status != 0 )); then
    local description

    # Let's be comprehensive
    case $last_exit_status in
      # Signals are 128 + signal value, we translate back to the signal name
      $((128 + 1))  ) description="SIGHUP";;
      $((128 + 2))  ) description="SIGINT";;
      $((128 + 3))  ) description="SIGQUIT";;
      $((128 + 4))  ) description="SIGILL";;
      $((128 + 5))  ) description="SIGTRAP";;
      $((128 + 6))  ) description="SIGABRT";;
      $((128 + 7))  ) description="SIGEMT";;
      $((128 + 8))  ) description="SIGFPE";;
      $((128 + 9))  ) description="SIGKILL";;
      $((128 + 10)) ) description="SIGBUS";;
      $((128 + 11)) ) description="SIGSEGV";;
      $((128 + 12)) ) description="SIGSYS";;
      $((128 + 13)) ) description="SIGPIPE";;
      $((128 + 14)) ) description="SIGALRM";;
      $((128 + 15)) ) description="SIGTERM";;
      $((128 + 16)) ) description="SIGURG";;
      $((128 + 17)) ) description="SIGSTOP";;
      $((128 + 18)) ) description="SIGTSTP";;
      $((128 + 19)) ) description="SIGCONT";;
      $((128 + 20)) ) description="SIGCHLD";;
      $((128 + 21)) ) description="SIGTTIN";;
      $((128 + 22)) ) description="SIGTTOU";;
      $((128 + 23)) ) description="SIGIO";;
      $((128 + 24)) ) description="SIGXCPU";;
      $((128 + 25)) ) description="SIGXFSZ";;
      $((128 + 26)) ) description="SIGVTALRM";;
      $((128 + 27)) ) description="SIGPROF";;
      $((128 + 28)) ) description="SIGWINCH";;
      $((128 + 29)) ) description="SIGINFO";;
      $((128 + 30)) ) description="SIGUSR1";;
      $((128 + 31)) ) description="SIGUSR2";;
      # All other cases. While ZSH may have specific errors for lack of permissions
      # the script itself may define these, to be conservative, just output the
      # number.
      *)              description="${last_exit_status}";;
    esac
    last_exit="%F{red}Exit code: $last_exit_status [${description}]%f "
  fi;
}

NEWLINE=$'\n'
if [[ $TERM = "linux" ]]
then
    PROMPT="[ %F{yellow}%n %F{white}@ %F{blue}%M %F{white}: %F{magenta}%d%f ] ${NEWLINE}  > "
else
    [[ -z "$TMUX" ]] && PROMPT="%K{0}%F{11}%f%k%K{11}%F{0} %n %f%k%K{39}%F{11}%f%k%K{39}%F{0} %M %f%k%K{97}%F{39}%f%k%K{97}%F{0} %d %f%k%K{0}%F{97}%f%k %F{8}%(1j.[bg: %j].)%f${NEWLINE}   └⮞ " || PROMPT="%F{1}%(1j.[bg: %j].) %F{45}⮞%f "
    #[[ -z "$TMUX" ]] && PROMPT="%K{0}%F{11}%f%k%K{11}%F{0} %n %f%k%K{39}%F{11}%f%k%K{39}%F{0} %M %f%k%K{97}%F{39}%f%k%K{97}%F{0} %d %f%k%K{0}%F{97}%f%k %F{8}%(1j.[bg: %j].)%f${NEWLINE}   └⮞ " || PROMPT="%F{11}%f%K{1}%F{15}%(1j.[bg: %j].)%f%k%K{11}%F{0} %n %f%k%F{11}%f "
fi

# ------------------
#autoload -Uz vcs_info
#precmd_vcs_info() { vcs_info }
#precmd_functions+=( precmd_vcs_info )
#setopt prompt_subst
#RPROMPT=\$vcs_info_msg_0_
##zstyle ':vcs_info:git:*' formats '%F{240}(%b) %r%f'
#zstyle ':vcs_info:git:*' formats '%F{240}(%b)%f'
#zstyle ':vcs_info:*' enable git
#-------------------

#source $ZDOTDIR/zsh-git-prompt/zshrc.sh # olivierverdier/zsh-git-prompt
source $ZPLUGINS/git-prompt.zsh
GIT_PROMPT_EXECUTABLE="haskell"
#ZSH_THEME_GIT_PROMPT_CACHE=""
RPROMPT='${last_exit}$(git_super_status)'

precmd() {
    precmd() {
	# print -Pn "\e]1;%m[%2~]\a" ; print -Pn "\e]2;%2~\a"  # Sets title to cwd
	print -Pn "\e]1;%m[%2~]\a" ; print -Pn "\e]2;%d\a"  # Sets title to cwd
        echo
    }
    #_exit_status
}
precmd_functions=("_exit_status" ${precmd_functions[@]})

# }}}
# Keyboard Fix {{{
autoload zkbd
[[ ! -f ${ZDOTDIR:-$HOME}/.zkbd/$TERM ]] && zkbd
source ${ZDOTDIR:-$HOME}/.zkbd/$TERM

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char
# }}}
# vi mode {{{
bindkey -v
export KEYTIMEOUT=1

# Change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[3 q'                 # Underline cursor for Normal and Visual mode
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'                 # Bar cursor for Insert mode
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # Use beam shape cursor on startup.
preexec() {
    echo -ne '\e[5 q'  # Use beam shape cursor for each new prompt.
    #print -Pn "\e]1;$1\a" ; print -Pn "\e]2;[%n@%m] %~ : $1\a" # Sets title to running process.
    print -Pn "\e]1;$1\a" ; print -Pn "\e]2;%2~ : $1\a" # Sets title to running process.
}
# }}}
# Functions + Aliases {{{
job_control() {
    [ $# -ne 1 ] && { echo 'What to do? Kill, Disown, Send to foreground or background?'; return 0; }
    case "$1" in
	'fg'    ) fg %$(jobs | fzf --reverse --height=20% --prompt="Bring to foreground: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'bg'    ) bg %$(jobs | fzf --reverse --height=20% --prompt="Continue in background: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'disown') disown %$(jobs | fzf --reverse --height=20% --prompt="Disown: " | sed "s/.*\[\([^]]*\)\].*/\1/g") ;;
	'kill'  ) kill $(jobs -l | fzf  --reverse --height=20% --prompt="Kill: " | awk '{print $3}') ;;
	 *      ) echo '1 arg: kill/disown/fg/bg' ;;
    esac
}
alias jf='job_control fg'
alias jb='job_control bg'
alias jd='job_control disown'
alias jk='job_control kill'
# ----------
lfcd() {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        [ -d "$dir" ] && [ "$dir" != "$(pwd)" ] && cd "$dir"
    fi
}
#-----------
function chnet() {
    con-pagla() {
	echo "Scanning networks"
	nmcli d wifi rescan
	sleep 2

	echo "Connecting to Pagla Dashu"
	nmcli d wifi connect "Pagla Dashu" hidden yes
    }
    con-antor() {
	echo "Scanning networks"
	nmcli d wifi rescan
	sleep 2

	echo "Connecting to ANTOR"
	nmcli d wifi connect "ANTOR"
    }
    if [ $# -ne 1 ]
    then
	echo 'Pass p/a/s as argument'
    else
	case $1 in
	    'p') con-pagla ;;
	    'a') con-antor ;;
	    's') nmcli connection show ;;
	    *) echo 'Pass p/a/s as argument' ;;
	esac
    fi
}
#-----------
reap() {
    local filen="$1"
    local foldern="${filen%.*}"
    7z x -o"$foldern" "$filen"
    cd "$foldern"
}
#-----------
mdc() {
    mkdir -p "$1"
    cd "$1"
}
#-----------
alias ls="ls --color"
alias grep='grep --colour'
alias tree='tree -C'
alias zf='z -I'
alias zb='z -b'
alias vfc="vf ~/.config"
alias vfs="vf ~/.local/scripts"
alias dotf='git --git-dir=/media/data-drive/dotfiles-repo --work-tree=$HOME'
alias manl='mimeo "/media/data-drive/Documents/manuals/$(ls /media/data-drive/Documents/manuals | fzf --reverse --height=20%)"'
# }}}
# External sources {{{
source /usr/share/doc/pkgfile/command-not-found.zsh

source $ZPLUGINS/fzf-tab.zsh
#source $ZPLUGINS/zsh-pyenv.zsh
#source $ZPLUGINS/completions.zsh
source $ZPLUGINS/abbr.zsh
source $ZPLUGINS/fzf-history.zsh
source $ZPLUGINS/suggestion.zsh
source $ZPLUGINS/syntax.zsh

eval "$(lua /usr/share/z.lua/z.lua --init zsh enhanced once)"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

source /usr/share/nvm/init-nvm.sh
source /usr/share/zsh/site-functions/_pyenv
fpath=(/home/asad/builds/sh-manpage-completions/completions/zsh $fpath)
# }}}
# vim: fdm=marker fdl=0

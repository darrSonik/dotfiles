#bleopt default_keymap=vi

# bleopt keymap_vi_nmap_cursor:=2
# bleopt keymap_vi_imap_cursor:=5
# bleopt keymap_vi_omap_cursor:=4
# bleopt keymap_vi_xmap_cursor:=2
# bleopt keymap_vi_cmap_cursor:=0

ble-bind -m vi_nmap --cursor 2
ble-bind -m vi_imap --cursor 5
ble-bind -m vi_omap --cursor 4
ble-bind -m vi_xmap --cursor 2
ble-bind -m vi_cmap --cursor 0

#function blerc/vim-load-hook {
#    ((_ble_bash>=40300)) && builtin bind 'set keyseq-timeout 1'
#
#    bleopt keymap_vi_mode_show=0
#    bleopt keymap_vi_nmap_cursor:=2
#    bleopt keymap_vi_imap_cursor:=5
#    bleopt keymap_vi_omap_cursor:=4
#    bleopt keymap_vi_xmap_cursor:=2
#    bleopt keymap_vi_cmap_cursor:=0
#}
#blehook/eval-after-load keymap_vi blerc/vim-load-hook

# Insert git branch name from menu using `\branch`
function blerc/define-sabbrev-branch {
  function blerc/sabbrev-git-branch {
    ble/util/assign-array COMPREPLY "git branch | sed 's/^\*\{0,1\}[[:space:]]*//'" 2>/dev/null
  }
  ble-sabbrev -m '\branch'=blerc/sabbrev-git-branch
}
blehook/eval-after-load complete blerc/define-sabbrev-branch

# Insert git commit ID from menu using `\commit`
function blerc/define-sabbrev-commit {
  ble/color/defface blerc_git_commit_id fg=navy
  ble/complete/action/inherit-from blerc_git_commit_id word
  function ble/complete/action:blerc_git_commit_id/init-menu-item {
    local ret
    ble/color/face2g blerc_git_commit_id; g=$ret
  }
  function blerc/sabbrev-git-commit {
    bleopt sabbrev_menu_style=desc-raw
    bleopt sabbrev_menu_opts=enter_menu

    local format=$'%h \e[1;32m(%ar)\e[m %s - \e[4m%an\e[m\e[1;33m%d\e[m'
    local arr; ble/util/assign-array arr 'git log --pretty=format:"$format"' &>/dev/null
    local line hash subject
    for line in "${arr[@]}"; do
      builtin read hash subject <<< "$line"
      ble/complete/cand/yield blerc_git_commit_id "$hash" "$subject"
    done
  }
  ble-sabbrev -m '\commit'='blerc/sabbrev-git-commit'
}
blehook/eval-after-load complete blerc/define-sabbrev-commit

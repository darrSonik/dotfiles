" WhichKey
let g:which_key_map.F = {
    \ 'name' : '+FZF'    ,
    \ 'H' : [':Help'     , 'Help']                    ,
    \ 'f' : [':Files'    , 'Files']                   ,
    \ 'b' : [':Buffers'  , 'Buffers']                 ,
    \ 'w' : [':Windows'  , 'Windows']                 ,
    \ 'l' : [':BLines'   , 'Lines in current buffer'] ,
    \ 'L' : [':Lines'    , 'Lines in loaded buffers'] ,
    \ 's' : [':Rg'       , 'Search text']             ,
    \ 'm' : [':Marks'    , 'Marks']                   ,
    \ 'S' : [':Snippets' , 'Snippets (UltiSnips)']    ,
    \ 'c' : [':Commits'  , 'Git commits']             ,
    \ 'C' : [':BCommits' , 'Git commits (Buffer)']    ,
    \ 'h' : [':History'  , 'File History']            ,
    \ ':' : [':History:' , 'Command History']         ,
    \ '/' : [':History/' , 'Search History']          ,
    \ }

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<C-j>"
let g:UltiSnipsJumpBackwardTrigger="<C-k>"
let g:UltiSnipsSnippetDirectories=["UltiSnips", "my_snippets"]

let g:UltiSnipsEditSplit="vertical"

" let g:coc_snippet_next = '<c-n>'
" let g:coc_snippet_prev = '<c-p>'

" imap <C-l> <Plug>(coc-snippets-expand)
" vmap <C-j> <Plug>(coc-snippets-select)
" imap <C-j> <Plug>(coc-snippets-expand-jump)

" WhichKey
let g:which_key_map.g = {
    \ 'name': '+Git',
    \ 'S' : [':tab G'     , 'Status (Tab)']         ,
    \ 's' : [':Git'       , 'Status (Split)']       ,
    \ 'w' : [':Gwrite'    , 'Save and add file']    ,
    \ 'c' : [':Git commit', 'Commit']               ,
    \ 'C' : [':Git commit -S', 'Commit (with Sign)'],
    \ 'p' : [':Git push'  , 'Push']                 ,
    \ }

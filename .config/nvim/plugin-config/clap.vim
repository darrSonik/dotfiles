let g:clap_theme = 'material_design_dark'

" WhichKey
let g:which_key_map.f = {
    \ 'name' : '+Clap',
    \ 'e' : [':Clap filer', 'Filer'],
    \ 'g' : {
	\ 'name' : '+Git',
	\ 'c' : [':Clap commits', 'Git commits'],
	\ 'b' : [':Clap bcommits', 'Git commits for current buffer'],
	\ 'f' : [':Clap gfiles', 'Files managed by Git'],
	\ 'd' : [':Clap git_diff_files', 'Uncommitted changes'],
	\ },
    \ 'h' : {
	\ 'name' : '+History',
	\ 'f' : [':Clap history', 'Recent files'],
	\ 's' : [':Clap search_history', 'Recent searches'],
	\ 'c' : [':Clap command_history', 'Recent commands'],
	\ },
    \ 'f' : {
	\ 'name' : '+Filter',
	\ 'l' : [':Clap blines', 'Lines in current buffer'],
	\ 'L' : [':Clap lines', 'Lines in loaded buffers'],
	\ 'g' : [':Clap grep', 'Grep'],
	\ 'G' : [':Clap grep2', 'Grep (dynamic)'],
	\ 'j' : [':Clap dumb_jump', 'References using regexp'],
	\ },
    \ 't' : {
	\ 'name' : '+Tags',
	\ 'b' : [':Clap tags', 'Tags in current buffer'],
	\ 'a' : [':Clap proj_tags', 'Tags in current project'],
	\ },
    \ 's' : {
	\ 'name' : '+Search',
	\ 'f' : [':Clap files', 'All Files'],
	\ 't' : [':Clap colors', 'Themes'],
	\ 'c' : [':Clap command', 'Commands'],
	\ 'h' : [':Clap help_tags', 'Help tags'],
	\ 'j' : [':Clap jumps', 'Jumps'],
	\ 'k' : [':Clap maps', 'Keybindings'],
	\ 'm' : [':Clap marks', 'Marks'],
	\ 'q' : [':Clap quickfix', 'Quickfix list'],
	\ 'l' : [':Clap loclist', 'Location list'],
	\ 'r' : [':Clap registers', 'Registers'],
	\ 'y' : [':Clap yanks', 'Yank stack'],
	\ },
    \ 'b' : {
	\ 'name' : '+Buffer/Window',
	\ 'w' : [':Clap windows', 'Window list'],
	\ 'b' : [':Clap buffers', 'Buffer list'],
	\ 't' : [':Clap filetypes', 'File Types'],
	\ },
    \ }

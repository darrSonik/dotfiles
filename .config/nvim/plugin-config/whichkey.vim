"set timeoutlen=1000

let g:which_key_sort_horizontal = 0
let g:which_key_sep = ''
let g:which_key_use_floating_win=0
let g:which_key_group_dicts = ''

let g:which_key_display_names = { ' ': 'SPC', '<TAB>': '⇆' }

" Change the colors if you want
highlight default link WhichKey          Operator
highlight default link WhichKeySeperator DiffAdded
highlight default link WhichKeyGroup     Identifier
highlight default link WhichKeyDesc      Function

" Hide status line
autocmd! FileType which_key
autocmd  FileType which_key set laststatus=0 noshowmode noruler
  \| autocmd BufLeave <buffer> set laststatus=2 noshowmode ruler

" Create map to add keys
let g:which_key_map = {}

let g:which_key_map.b = {
    \ 'name': '+Buffers',
    \ 'q' : [':bd'      , 'Close Buffer']             ,
    \ 'Q' : [':tabclose', 'Close Tab']                ,
    \ 'n' : [':bn!'     , 'Next Buffer (<TAB>)']      ,
    \ 'p' : [':bp!'     , 'Previous Buffer(<S-TAB>)'] ,
    \ 't' : ['gt'       , 'Switch tabs(<M-TAB>)']     ,
    \ 'w' : [':up'      , 'Save file (if changed)']   ,
    \ 'W' : [':W'       , 'Save file (with sudo)']    ,
    \ }

let g:which_key_map.w = {
    \ 'name' : '+Windows/Splits',
    \ '-' : ['<C-W>s'                 , 'Split Horizontal']                ,
    \ '|' : ['<C-W>v'                 , 'Split Vertical' ]                 ,
    \ 'h' : ['<C-w>h'                 , 'Left Split (<C-h>)']              ,
    \ 'l' : ['<C-w>l'                 , 'Right Split<C-l>)']               ,
    \ 'j' : ['<C-w>j'                 , 'Below Split<C-j>)']               ,
    \ 'k' : ['<C-w>k'                 , 'Up Split(C-k)']                   ,
    \ 'H' : ['<C-W>5<'                , 'Expand window-left(<C-up>)']      ,
    \ 'J' : [':resize +5'             , 'Expand window-below(<C-Right>)']  ,
    \ 'L' : ['<C-W>5>'                , 'Expand window-right(<C-Left>)']   ,
    \ 'K' : [':resize -5'             , 'Expand window-up(<C-Down>)']      ,
    \ '+' : ['<C-w>_<C-w>|'           , 'Maximise window(Z-)']             ,
    \ '=' : ['<C-W>='                 , 'Balance window(Z=)']              ,
    \ 'm' : [':call MarkWindowSwap()' , 'Mark Window for Swapping']        ,
    \ 's' : [':call DoWindowSwap()'   , 'Swap current with Marked Window'] ,
    \ }

" let g:which_key_map.t = {'name' : '+NetRW'}
let g:which_key_map.e = 'NetRW'
let g:which_key_map.T = 'Tags in sidebar'

let g:which_key_map.t = {
    \ 'name' : '+Terminal',
    \ 'h' : [':split | terminal' , 'Horizontal Split Terminal'],
    \ 'v' : [':vsplit | terminal', 'Vertical Split Terminal']
    \ }

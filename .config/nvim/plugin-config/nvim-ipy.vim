command! -nargs=0 RunQtConsole
  \ call jobstart("jupyter qtconsole --JupyterWidget.include_other_output=True")

let g:ipy_celldef = '^##' " regex for cell start and end

"nmap <silent> <leader>jqt :RunQtConsole<Enter>
"nmap <silent> <leader>jk :IPython<Space>--existing<Space>--no-window<Enter>
"nmap <silent> <leader>jc <Plug>(IPy-RunCell)
"nmap <silent> <leader>ja <Plug>(IPy-RunAll)

let g:which_key_map.J = {
    \ 'name' : '+Jupyter_QT',
    \ 'q' : ['RunQtConsole',                   'Open QT Console'],
    \ 'k' : [':IPython --existing --no-window', 'Connect to kernel'],
    \ 'c' : ['<Plug>(IPy-RunCell)',            'Run Cell'],
    \ 'a' : ['<Plug>(IPy-RunAll)',             'Run All Cells'],
    \ }

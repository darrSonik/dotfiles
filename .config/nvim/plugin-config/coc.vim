" TextEdit might fail if hidden is not set.
set hidden

" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Give more space for displaying messages.
"set cmdheight=2

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
" <cr> could be remapped by other vim plugin, try `:verbose imap <CR>`.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif

" Use K to show documentation in preview window.
nnoremap <silent> <localleader>d :call <SID>show_documentation()<CR>
nnoremap <silent> <localleader>a <Plug>(coc-codelens-action)
" nnoremap <localleader>d :call <SID>show_documentation()<CR>


function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocActionAsync('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fol current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" :: changed <leader> to <space> Since WhichKey didn't recognise
" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <space>a  <Plug>(coc-codeaction-selected)

" Formatting selected code.
xmap <space>f  <Plug>(coc-format-selected)

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
"nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

let g:which_key_map.c = { 
    \ 'name': '+CoC',
    \ 'A' : ['<Plug>(coc-codelens-action)', 'CodeLens Actions'],
    \ 'e' : [':CocList extensions', 'Extensions'],
    \ 'c' : [':CocList commands', 'Commands'],
    \ 'd' : {
	\ 'name' : '+Diagnostics',
	\ 'i' : ['<Plug>(coc-diagnostic-info)', 'Info (Hover)'],
	\ 'a' : [':CocList diagnostics', 'All diagnostics'],
	\ 'n' : ['<Plug>(coc-diagnostic-next)', 'Next diagnostic'],
	\ 'p' : ['<Plug>(coc-diagnostic-prev)', 'Previous diagnostic'],
	\ },
    \ 'v' : ['<Plug>(coc-range-select)', 'Select Range'],
    \ 'a' : {
	\'name': '+Actions',
	\ 'a' : ['<Plug>(coc-codeaction)', 'Apply codeAction to current buffer'],
	\ 'f' : ['<Plug>(coc-fix-current)', 'Apply AutoFix on current line'],
	\ 'F' : ['<Plug>(coc-format-selected)', 'Format selected code'],
	\ 'n' : [':CocNext', 'Default action for next item'],
	\ 'p' : [':CocPrev', 'Default action for previous item'],
	\ 'x' : ['<Plug>(coc-codeaction-selected)', 'Apply action on selection'],
	\ },
    \ 'g' : {
	\ 'name' : '+Navigation',
	\ 'd' : ['<Plug>(coc-definition)', 'Go to definition'],
	\ 't' : ['<Plug>(coc-type-definition)', 'Go to Type definition'],
	\ 'i' : ['<Plug>(coc-implementation)', 'Go to implementation'],
	\ 'r' : ['<Plug>(coc-references)', 'Go to References'],
	\ },
    \ 's' : {
	\ 'name' : '+Symbols',
	\ 's' : [':CocList -I symbols', 'Search Symbols'],
	\ 'o' : [':CocList outline', 'Symbols in current buffer'],
	\ 'r' : ['<Plug>(coc-rename)', 'Rename Symbol'],
	\ },
    \ 'D' : [':<SNR>25_show_documentation()<CR>', 'Documentation, Use (<localleader>d)'],
    \ 'p' : [':CocListResume', 'Resume latest CocList'],
    \ 'm' : [':CocList marketplace', 'Marketplace'],
    \ }

let b:coc_local_whichkey = {}
let b:coc_local_whichkey.d = 'Show documentation'
let b:coc_local_whichkey.a = 'CodeLens Action'

autocmd VimEnter * call which_key#register('\', "b:coc_local_whichkey")

" set statusline=%F%m%r%h%w%=(%{&ff}/%Y)\ (line\ %l\/%L,\ col\ %c)\
" let g:airline_theme = 'powerlineish'
" let g:airline_statusline_ontop=1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:airline_skip_empty_sections = 1
"let g:airline#extensions#tagbar#enabled = 1 -- Install TagBar Plugin First

let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''

let g:airline#extensions#tabline#left_sep=' '
let g:airline#extensions#tabline#right_sep=' '
let g:airline#extensions#tabline#left_alt_sep='|'
let g:airline#extensions#tabline#right_alt_sep='|'
let g:airline#extensions#branch#prefix     = '⎇' "➔, ➥, ⤴
let g:airline#extensions#readonly#symbol   = '⊘'
let g:airline#extensions#linecolumn#prefix = '¶'
let g:airline#extensions#paste#symbol      = 'ρ'
"let g:airline_symbols.readonly = ''
"let g:airline_symbols.linenr    = ''
"let g:airline_symbols.branch    = '⎇'
"let g:airline_symbols.paste     = 'ρ'
"let g:airline_symbols.paste     = 'Þ'
"let g:airline_symbols.paste     = '∥'
"let g:airline_symbols.whitespace = 'Ξ'

" Word Count
let g:airline#extensions#wordcount#enabled = 1
let g:airline#extensions#wordcount#filetypes = '\vasciidoc|help|mail|markdown|org|tex|text|wiki|vimwiki'
set laststatus=2

" disable TMUXLINE theme override
let g:airline#extensions#tmuxline#enabled = 0

let g:vimwiki_folding='expr'
au! FileType vimwiki :call StrikeVimwiki()
au! BufRead,BufNewFile *.md setlocal filetype=markdown

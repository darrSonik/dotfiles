" Disable default mappings
let g:EasyMotion_do_mapping = 0

" Handled by whichkey
"map _ <Plug>(easymotion-prefix)

let g:which_key_map.j = {
    \ 'name' : '+EasyMotion',
    \ 'f' : ['<Plug>(easymotion-f)', 'to character, Forward'],
    \ 'F' : ['<Plug>(easymotion-F)', 'to character, Backward'],
    \ 't' : ['<Plug>(easymotion-t)', 'till character, Forward'],
    \ 'T' : ['<Plug>(easymotion-T)', 'till character, Backward'],
    \ 'l' : ['<Plug>(easymotion-fl)', 'to character forward, inline'],
    \ 'h' : ['<Plug>(easymotion-Fl)', 'to character backward, inline'],
    \ 'j' : ['<Plug>(easymotion-j)', 'Jump to line (down)'],
    \ 'k' : ['<Plug>(easymotion-k)', 'Jump to line (up)'],
    \ 'w' : ['<Plug>(easymotion-bd-w)', 'word, bidirectional'],
    \ 'W' : ['<Plug>(easymotion-bd-W)', 'WORD, bidirectional'],
    \ 's' : ['<Plug>(easymotion-s2)', 'to character, bidirectional'],
    \ 'S' : ['<Plug>(easymotion-sn)', 'to multicharacter, bidirectional'],
    \ 'r' : ['<Plug>(easymotion-repeat)', 'Repeat last motion'],
    \ }

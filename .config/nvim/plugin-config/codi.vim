" Configuration for codi.vim
" look at https://github.com/metakirby5/codi.vim/blob/master/doc/codi.txt

let g:codi#interpreters = {
    \ 'python': {
	\ 'bin': 'python',
	\ 'prompt': '^\(>>>\|\.\.\.\) ',
    \ },
    \ 'javascript': {
	\ 'rightalign': 0,
    \ },
\ }

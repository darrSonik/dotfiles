setlocal spell spelllang=en_gb
setlocal tabstop=4
setlocal conceallevel=2
nmap <localleader>c :<C-u>call ToggleConceal()<cr>

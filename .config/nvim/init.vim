" Custom Functions {{{
function Exec_Return(command)
    redir =>output
    silent exec a:command
    redir END
    return output
endfunction

" Toggle Highlights
function! ToggleHLSearch()
    if &hls
	set nohls
    else
	set hls
    endif
endfunction

" Toggle Conceal Level
function! ToggleConceal()
    if &conceallevel == 0
	setlocal conceallevel=2
    else
	setlocal conceallevel=0
    endif
endfunction

" Enable Conceal
function EnableConceal()
    set foldlevel=999
    setlocal conceallevel=2
    " highlight clear Conceal
endfunction

" Strikethrough VimWiki
function StrikeVimwiki()
    highlight Crossvwk gui=strikethrough cterm=strikethrough
    match Crossvwk /\~\~.*\~\~/
endfunction

" Counts word in file.
function WordCount()
    let s:old_status = v:statusmsg
    exe "silent normal g\<c-g>"
    let s:word_count = str2nr(split(v:statusmsg)[11])
    let v:statusmsg = s:old_status
    return s:word_count
endfunction

" For Swapping Splits
function! MarkWindowSwap()
    let g:markedWinNum = winnr()
endfunction

function! DoWindowSwap()
    "Mark destination
    let curNum = winnr()
    let curBuf = bufnr( "%" )
    exe g:markedWinNum . "wincmd w"
    "Switch to source and shuffle dest->source
    let markedBuf = bufnr( "%" )
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' curBuf
    "Switch to dest and shuffle source->dest
    exe curNum . "wincmd w"
    "Hide and open so that we aren't prompted and keep history
    exe 'hide buf' markedBuf 
endfunction

" Create directory and save file if it doesn't exist
function s:MkNonExDir(file, buf)
    if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
        let dir=fnamemodify(a:file, ':h')
        if !isdirectory(dir)
            call mkdir(dir, 'p')
        endif
    endif
endfunction
augroup BWCCreateDir
    autocmd!
    autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END

" }}}
" Sensible defaults {{{
set title
set nocompatible
set shell=bash
filetype plugin indent on
syntax on
set termguicolors
colorscheme slate
set number relativenumber

set encoding=utf-8
set fileencoding=utf-8
set fileencodings=utf-8

set autoread
"set autowrite
"set autowriteall

"set ruler
set modeline
set modelines=5

" Copy to system clipboard
set clipboard=unnamedplus

" Cursor
set guicursor=
" Blinking Cursor when leaving vim - it stops blinking if I don't do this
"au VimLeave * set guicursor=a:block-blinkon150
set guicursor=i:ver25,n:block,v-ve-r:hor15,a:blinkon1
" highlight Cursor guifg=white guibg=black
" highlight iCursor guifg=white guibg=steelblue
" set cursorline
" set cursorcolumn

" Set terminal cursor to vertical line when leaving vim
augroup Shape
    autocmd!
    autocmd VimLeave * set guicursor=a:ver90
augroup END

" Indents - Tabs and Spaces
set tabstop=8
set shiftwidth=4
set softtabstop=4
" set expandtab
set smarttab

" Enable side-scrolling and don't wrap
set nowrap sidescroll=1

" Mouse
set mouse=a
set mousefocus
set mousehide
"set mousemodel=popup

" Set search highlight options
set ignorecase
set smartcase
set hlsearch
set incsearch
autocmd InsertEnter * :setlocal nohlsearch
autocmd InsertLeave * :setlocal hlsearch

" Fix popup colour
highlight Pmenu guibg=#5e644f
highlight PmenuSel guifg=Black

" Comments in italic
"highlight Comment cterm=italic gui=italic
" I don't need to use `cterm` though

lua << EOF
vim.cmd 'highlight Comment cterm=italic gui=italic'
EOF

" No Background - Use Terminal background
hi! Normal guibg=NONE ctermbg=NONE
hi! NonText guibg=NONE ctermbg=NONE
" }}}
" Custom Keybindings {{{
"let mapleader=' '
let mapleader=' '
let maplocalleader='_'

" WhichKey for leader keybindings
" nnoremap <silent> <leader>      :silent WhichKey '<Space>'<CR>
nnoremap <silent> <leader>      :silent <c-u>WhichKey '<Space>'<CR>
vnoremap <silent> <leader>      :silent <c-u>WhichKeyVisual '<Space>'<CR>
nnoremap <silent> <localleader> :silent <c-u>WhichKey '_'<CR>
vnoremap <silent> <localleader> :silent <c-u>WhichKeyVisual '_'<CR>

" Buffer Manipulation
nnoremap <TAB> :bn!<CR>
nnoremap <S-TAB> :bp!<CR>
nnoremap <M-TAB> gt
"nnoremap <M-TAB> tabprev
"nnoremap <M-TAB> tabnext

" Open NetRW
nnoremap <silent> <leader>e :Lexplore<CR>
nnoremap <silent> <leader>T :Vista!!<CR>

" Clear search
noremap <silent> <C-M-\> <Esc>:let @/ = ""<CR>
" Turn off highlights
" noremap <silent> <C-M-\> :call ToggleHLSearch()<CR>

" Press Esc in terminal mode insted of (C-\ +  C-n)
au TermOpen * tnoremap <Esc> <c-\><c-n>
"au FileType fzf tunmap <Esc>

" Splits - Swtiching
map <silent> <left> <C-w>h
map <silent> <down> <C-w>j
map <silent> <up> <C-w>k
map <silent> <right> <C-w>l
" Splits - Resizing
nmap <C-Up> :res +5<CR>
nmap <C-Down> :res -5<CR>
nmap <C-Right> :vert res +5<CR>
nmap <C-Left> :vert res -5<CR>
" Split Zoom
noremap Z- <c-w>_ <Bar> <c-w>\|
noremap Z= <c-w>=
" Split - Swapping
nnoremap Zm :call MarkWindowSwap()<CR>
nnoremap Zs :call DoWindowSwap()<CR>

" Fix Spelling Mistakes on the fly
inoremap <C-\> <c-g>u<Esc>[s1z=`]a<c-g>u

" Enable Conceal
" nmap <silent> <C-M-0> :call EnableConceal()<CR>

" Vmap for maintain Visual Mode after shifting > and <
vmap < <gv
vmap > >gv

" Move visual block
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv
" Moving texts
nnoremap <silent> <localleader>j :m .+1<CR>==
nnoremap <silent> <localleader>k :m .-2<CR>==

" Paste the last yanked text
nmap Zp "0p
nmap ZP "0P

" y acting as C, D instead of yanking whole line
nmap Y y$

" Keep the cursor centred
nnoremap n nzzzv
nnoremap N Nzzzv
nnoremap J mzJ`z

" Undo break points
"inoremap , ,<c-g>u " creates a undo break point after every comma I type

" Autoclose quotes, brackets
inoremap " ""<left>
inoremap "<BS> "

inoremap ' ''<left>
inoremap '<BS> '

inoremap ` ``<left>
inoremap `<BS> `

inoremap ( ()<left>
inoremap () ()
inoremap (; ();<left><left>

inoremap [ []<left>
inoremap [] []

inoremap { {}<left>
inoremap {} {}
inoremap {<CR> {<CR>}<ESC>O

" Remove trailing spaces
" TODO: add a autocmd to call this on file save
nmap <silent> <C-M-r> :%s/\s\+$//e<CR>
command! FixWhitespace :%s/\s\+$//e

" Open VIMRC
command! VC execute ":edit $MYVIMRC"

" Reload Vim Config
command! RC execute "source $MYVIMRC"

" Write File as SUDO
command! W execute ":SudaWrite"

" Change Working Directory according to current file
command! CD execute ":cd %:p:h"

" Change Working Directory according to current file (local to window)
command! LD execute ":lcd %:p:h"


" Omnicomplete navigation with JK
" inoremap <expr> <C-j> ("\<C-n>")
" inoremap <expr> <C-k> ("\<C-p>")
" }}}
" Custom Abbreviations {{{
" I BETTER BE USING `UltiSnips`
" Load my custom abbreviations - `runtime` doesn't work, using `source` for a dedicated snippets file
" source $HOME/.config/nvim/abbreviations/custom-abbreviations.vim
" Also use snippets on certain filetype
" autocmd! BufRead,BufNewFile *.tex source $HOME/.config/nvim/abbreviations/latex.vim

" Print Date and Time
"iab pdate strftime('%d %b %Y')
"iab ptime strftime('%T')
"iab pdt strftime('%c')
" }}}
" Code Folding {{{
set foldmethod=syntax
" set nofoldenable
set foldlevel=20

" let c_fold=1
let g:markdown_folding=1
let g:asciidoc_folding=1

" Save and restore manual folds when we exit a file
augroup SaveManualFolds
    autocmd!
    au BufWinLeave, BufLeave ?* silent! mkview
    au BufWinEnter           ?* silent! loadview
augroup END
" }}}
" Conceal {{{
" Enable concealing
" setlocal conceallevel=1
" setlocal concealcursor=nvic "not needed
"
" Clear conceal highlights
" hi clear Conceal
"
" Or change highlight colour to NONE
highlight Conceal guifg=Red guibg=NONE
"
" add your conceals in after/syntax/<filetype>.vim file
" }}}
" Extensions {{{
let $pcdir = '/home/asad/.config/nvim/plugin-config'
call plug#begin('~/.config/nvim/plugged')

Plug 'liuchengxu/vim-which-key'
source $pcdir/whichkey.vim

Plug 'vim-airline/vim-airline'
source $pcdir/airline.vim

" Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" Plug 'junegunn/fzf.vim'
" source $pcdir/fzf.vim

" Plug 'easymotion/vim-easymotion'
" source $pcdir/easymotion.vim

Plug 'justinmk/vim-sneak'
source $pcdir/sneak.vim

Plug 'junegunn/vim-easy-align'
source $pcdir/easyalign.vim

" Plug 'ms-jpq/chadtree', {'branch': 'chad', 'do': ':UpdateRemotePlugins'}
" source $pcdir/chadtree.vim

" Plug 'liuchengxu/vim-clap', { 'do': ':Clap install-binary' }
" source $pcdir/clap.vim

Plug 'tpope/vim-fugitive'
source $pcdir/fugitive.vim

Plug 'neoclide/coc.nvim', {'branch': 'release'}
source $pcdir/coc.vim
" Plug 'vn-ki/coc-clap'

" Plug 'ldelossa/litee.nvim'

" Plug 'codota/tabnine-vim' "-- installed a CoC extension
" source $pcdir/tabnine.vim

Plug 'liuchengxu/vista.vim'
source $pcdir/vista.vim

" Plug 'honza/vim-snippets'
Plug 'sirver/ultisnips'
source $pcdir/ultisnips.vim

Plug 'lambdalisue/suda.vim'
source $pcdir/suda.vim

Plug 'mbbill/undotree'
source $pcdir/undotree.vim

" Plug 'puremourning/vimspector'
" source $pcdir/vimspector.vim

" Plug 'dhruvasagar/vim-table-mode'
" source $pcdir/tablemode.vim

" Plug 'metakirby5/codi.vim'
" source $pcdir/codi.vim

"Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

Plug 'KabbAmine/vCoolor.vim'
source $pcdir/vCoolor.vim

""Plug 'ptzz/lf.vim'
""Plug 'voldikss/vim-floaterm'
""source $pcdir/lf+floaterm.vim

"source $pcdir/floaterm.vim

Plug 'ap/vim-css-color'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'elixir-lang/vim-elixir'
Plug 'psliwka/vim-smoothie'
" Plug 'dag/vim-fish'

Plug 'evanleck/vim-svelte'
Plug 'cespare/vim-toml'
Plug 'dart-lang/dart-vim-plugin'
Plug 'digitaltoad/vim-pug'

" NeoVim for browser inputs
"Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }

call plug#end()
" }}}
" NETRW {{{
function! NetrwRemoveRecursive()
    if &filetype ==# 'netrw'
	cnoremap <buffer> <CR> rm -r<CR>
	normal mu
	normal mf

	try
	    normal mx
	catch
	    echo "Canceled"
	endtry

	cunmap <buffer> <CR>
    endif
endfunction


function! NetrwMapping()
    nmap <buffer> H u
    nmap <buffer> h -^
    nmap <buffer> l <CR>
    nmap <buffer> <silent> L <CR>:Lexplore<CR>
    nmap <buffer> P <C-w>z
    
    nmap <buffer> . gh

    nmap <buffer> <TAB> mf
    nmap <buffer> <S-TAB> mF
    nmap <buffer> <C-TAB> mu

    nmap <buffer> bb mb
    nmap <buffer> bd mB
    nmap <buffer> bj gb

    nmap <buffer> ff %:w<CR>:buffer #<CR>
    nmap <buffer> fe R
    nmap <buffer> fc mc
    nmap <buffer> fC mtmc
    nmap <buffer> fx mm
    nmap <buffer> fX mtmm
    nmap <buffer> f; mx

    nmap <buffer> fl :echo join(netrw#Expose("netrwmarkfilelist"), "\n")<CR>
    nmap <buffer> fq :echo 'Target:' . netrw#Expose("netrwmftgt")<CR>
    nmap <buffer> fd mtfq

    nmap <buffer> FF :call NetrwRemoveRecursive()<CR>


    " Extra Keybindings for which-key
    nmap <buffer> +h gh
    nmap <buffer> +mm mf
    nmap <buffer> +mu mF
    nmap <buffer> +mU mu
    nmap <buffer> +bm mb
    nmap <buffer> +bd mB
    nmap <buffer> +bj gb
    nmap <buffer> +ff %:w<CR>:buffer #<CR>
    nmap <buffer> +fF d
    nmap <buffer> +fr R
    nmap <buffer> +fc mc
    nmap <buffer> +fC mtmc
    nmap <buffer> +fm mm
    nmap <buffer> +fM mtmm
    nmap <buffer> +f; mx
    nmap <buffer> +fl :echo join(netrw#Expose("netrwmarkfilelist"), "\n")<CR>
    nmap <buffer> +fq :echo 'Target:' . netrw#Expose("netrwmftgt")<CR>
    nmap <buffer> +fQ mtfq
    nmap <buffer> +fd D
    nmap <buffer> +fD :call NetrwRemoveRecursive()<CR>

    let g:whichkey_netrw = {}
    let g:whichkey_netrw.h = 'Show dotfiles (.)'
    let g:whichkey_netrw.m = {
	\ 'name' : '+Marks',
	\ 'm' : 'Toogle Mark (<TAB>)',
	\ 'u' : 'Unmark all in current buffer (<S-TAB>)',
	\ 'U' : 'Remove all marks (<C-TAB>)',
	\ }
    let g:whichkey_netrw.b = {
	\ 'name' : '+Bookmarks',
	\ 'm' : 'Create a bookmark (bb)',
	\ 'd' : 'Remove latest bookmark (bd)',
	\ 'j' : 'Jump to latest bookmark (bj)',
	\ }
    let g:whichkey_netrw.f = {
	\ 'name' : '+File_Operations',
	\ 'f' : 'Create file (% or ff)',
	\ 'F' : 'Create directory (d)',
	\ 'r' : 'Rename file (fe)',
	\ 'c' : 'Copy files to target-dir (fc)',
	\ 'C' : 'Copy files to dir-on-cursor (fC)',
	\ 'm' : 'Move files to target-dir (fx)',
	\ 'M' : 'Move files to dir-on-cursor (fX)',
	\ ';' : 'Run external cmd on the marked files (f;)',
	\ 'l' : 'List of marked files (fl)',
	\ 'q' : 'Show target-dir (fq)',
	\ 'Q' : 'Mark and show target-dir',
	\ 'd' : 'Delete (D)',
	\ 'D' : 'Delete recursively (FF)',
	\ }

    nnoremap <buffer> <silent> + :silent WhichKey '+'<CR>
    call which_key#register('+', 'g:whichkey_netrw')
endfunction



let g:netrw_banner = 0
let g:netrw_liststyle = 3

let g:netrw_keepdir = 0
let g:netrw_sort_options = 'i'

let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25

let g:netrw_localcopydircmd = 'cp -r'

let g:netrw_list_hide = '\(^\|\s\s\)\zs\.\S\+'

hi! link netrwMarkFile Search

augroup netrw_mapping
    autocmd!
    autocmd filetype netrw call NetrwMapping()
augroup END

" augroup ProjectDrawer
"     autocmd!
"     autocmd VimEnter * :Vexplore
" augroup END

" Look at the Brave bookmark for more customisation for NetRW
" }}}
" TreeSitter {{{
" lua <<EOF
" require'nvim-treesitter.configs'.setup {
"   ensure_installed = "all",     -- one of "all", "language", or a list of languages
"   highlight = {
"     enable = true,              -- false will disable the whole extension
"   },
" }
" EOF
" }}}
" LITEE {{{
" lua << EOF
" require('litee').setup({})
" EOF
" }}}
" vim:foldmethod=marker
" vim: foldlevel=0

"syntax match operator '*' conceal cchar=×
"syntax match operator '/' conceal cchar=÷
"syntax match Operator '=' conceal cchar=
syntax match Operator '=' conceal cchar=≔

syntax match Operator '&&' conceal cchar=∧
syntax match Operator '||' conceal cchar=∨
syntax match Operator '!'  conceal cchar=¬

syntax match Operator '==' conceal cchar=⇆
syntax match Operator '!=' conceal cchar=≠
syntax match Operator '>=' conceal cchar=⩾
syntax match Operator '<=' conceal cchar=⩽

"syntax match operator '~'  conceal cchar=¬
syntax match Operator '>>' conceal cchar=»
syntax match Operator '<<' conceal cchar=«

syntax match Operator '->' conceal cchar=

" syntax match ArrowHead contained '>' conceal cchar=契
" syntax match ArrowTail contained '-' conceal cchar=━
" syntax cluster cParenGroup add=ArrowTail,ArrowHead
" syntax match ArrowFull '->' contains=ArrowHead,ArrowTail

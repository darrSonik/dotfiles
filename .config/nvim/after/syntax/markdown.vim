"syntax match [Group] [regex]{ms=int,me=int} conceal cchar=[single char]

"syn region markdownLinkText matchgroup=markdownLinkTextDelimiter
"    \ start="!\=\[\%(\_[^]]*]\%( \=[[(]\)\)\@=" end="\]\%( \=[[(]\)\@="
"    \ nextgroup=markdownLink,markdownId skipwhite
"    \ contains=@markdownInline,markdownLineStart
"    \ concealends
"

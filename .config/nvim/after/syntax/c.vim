"syntax match operator '*' conceal cchar=×
"syntax match operator '/' conceal cchar=÷
syntax match Operator '=' conceal cchar=

syntax match Operator '&&' conceal cchar=∧
syntax match Operator '||' conceal cchar=∨
syntax match Operator '!'  conceal cchar=¬

syntax match Operator '==' conceal cchar=⇆ 
syntax match Operator '!=' conceal cchar=≠
syntax match Operator '>=' conceal cchar=⩾
syntax match Operator '<=' conceal cchar=⩽

"syntax match operator '~'  conceal cchar=¬
syntax match Operator '>>' conceal cchar=»
syntax match Operator '<<' conceal cchar=«

"syntax match operator '->' conceal cchar=

local tym = require('tym')

tym.set_config({
    -- font              = 'CaskaydiaCove Nerd Font Mono 10',
    font              = 'FantasqueSansMono Nerd Font Mono 12',
    cursor_shape      = 'ibeam',
    cursor_blink_mode = 'on',
})

tym.set_keymaps({
    ['<Ctrl>Up'] = function()
	tym.set('scale', tym.get('scale') + 10)
    end,
    ['<Ctrl>Down'] = function()
	tym.set('scale', tym.get('scale') - 10)
    end,
    ['<Ctrl>Home'] = function()
	tym.set('scale', 100)
    end,
})

local bg = '#161821'
local fg = '#c6c8d1'
return {
    color_background = bg,
    color_foreground = fg,
    color_bold = fg,
    color_highlight = fg,
    color_highlight_foreground = bg,
    
    color_0  = '#010302', -- black
    color_1  = '#c80815', -- red
    color_2  = '#228b22', -- green
    color_3  = '#cd7f32', -- brown
    color_4  = '#1560bd', -- blue
    color_5  = '#7851a9', -- purple
    color_6  = '#009494', -- cyan
    color_7  = '#c0c0c0', -- light gray
    color_8  = '#36454f', -- gray
    color_9  = '#ff0800', -- light red
    color_10 = '#32cd32', -- light green
    color_11 = '#f4c430', -- yellow
    color_12 = '#6495ed', -- light blue
    color_13 = '#fe1493', -- pink
    color_14 = '#00b7eb', -- light cyan
    color_15 = '#fffafa', -- white
}

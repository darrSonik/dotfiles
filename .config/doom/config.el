;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Asaduzzaman Noor"
      user-mail-address "mail-me@asaduzzaman-noor.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom. Here
;; are the three important ones:
;;
;; + `doom-font'
;; + `doom-variable-pitch-font'
;; + `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;;
;; They all accept either a font-spec, font string ("Input Mono-12"), or xlfd
;; font string. You generally only need these two:
;; (setq doom-font (font-spec :family "monospace" :size 12 :weight 'semi-light)
;;       doom-variable-pitch-font (font-spec :family "sans" :size 13))

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

(after! doom-themes
  (setq doom-themes-enable-bold t)
  (setq doom-themes-enable-italic t))

;; Changed default font
(setq doom-font (font-spec :family "FantasqueSansMono Nerd Font Mono" :size 16)
      doom-variable-pitch-font (font-spec :family "Nunito" :size 16)
      doom-big-font (font-spec :family "Comfortaa" :size 24))

;; Comments in Italics
(custom-set-faces! '(font-lock-comment-face :slant italic))

;; Modeline fonts
(setq doom-modeline-height 1)

(custom-set-faces
  '(mode-line ((t (:family "Noto Sans" :height 0.9))))
  '(mode-line-active ((t (:family "Noto Sans" :height 0.9))))
  '(mode-line-inactive ((t (:family "Noto Sans" :height 0.9)))))

(setq all-the-icons-scale-factor 1.2)

;; Custom Banner
(setq fancy-splash-image "~/.config/doom/asd-logo.png")

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
;(setq display-line-numbers-type t)
(setq display-line-numbers-type 'relative)

;; Here are some additional functions/macros that could help you configure Doom:
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Use tabs for indentation by default
(setq-default indent-tabs-mode t)

;; Copy to System Clipboard
(defun copy-to-clipboard ()
  "Copies selection to clipboard."
  (interactive)
  (if (display-graphic-p)
      (progn
        (message "Yanked region to x-clipboard.")
        (call-interactively 'clipboard-kill-ring-save))
    (if (region-active-p)
        (progn
          (shell-command-on-region (region-beginning) (region-end) "xclip -selection clipboard")
          (message "Yanked region to clipboard.")
          (deactivate-mark))
      (message "No region active; can't yank to clipboard."))))


;; Paste from System Clipboard
(defun paste-from-clipboard ()
  "Pastes from clipboard."
  (interactive)
  (if (display-graphic-p)
      (progn
        (clipboard-yank)
        (message "graphics active"))
    (insert (shell-command-to-string "xclip -selection clipboard -o"))))

; for smooth-scrolling
;(good-scroll-mode 1) ; leveraging good-scroll package
;(require 'sublimity)
;(require 'sublimity-scroll)
;(sublimity-mode 1)

(setq projectile-known-projects-file "~/.config/emacs/projectile.projects")

;; Neotree
(setq neo-smart-open t) ; point at currently opened file
(setq projectile-switch-project-action 'neotree-projectile-action) ; automatically switch neotree directory with projectile

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "/media/data-drive/Documents/emacs-org/"
      org-agenda-files '("/media/data-drive/Documents/emacs-org/agenda.org" "/media/data-drive/Documents/emacs-org/project-todos.org")
      org-default-notes-file (expand-file-name "notes.org" org-directory))

(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.5))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.4))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.3))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.2))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.1)))))

(setq org-hide-leading-stars t
      org-indent-indentation-per-level 4
      org-ellipsis " ▼ "
      ;org-superstar-headline-bullets-list '("◉" "○" "●" "◇" "◆" "◈")
      org-superstar-headline-bullets-list '("❖" "◈" "⚂" "⚃" "⚄" "⚅")
      org-superstar-item-bullet-alist '((?+ . ?▸) (?- . ?▹)) ; changes +/- symbols in item lists
      org-hide-emphasis-markers t)

(add-hook 'org-mode-hook (lambda ()
                           "Beautify Org Checkbox Symbol"
                           (push '("[ ]" . "☐") prettify-symbols-alist)
                           (push '("[X]" . "✓") prettify-symbols-alist)
                           (push '("[-]" . "ʘ") prettify-symbols-alist)
                           (prettify-symbols-mode)))


(defface org-checkbox-done-text
  '((t (:forground "#71696A" :strike-through t)))
  "Face for the text part of a checked org-mode checkbox")

(font-lock-add-keywords
  'org-mode
  `(("^[ \t]*\\(?:[-+*]\\|[0-9]+[).]\\)[ \t]+\\(\\(?:\\[@\\(?:start:\\)?[0-9]+\\][ \t]*\\)?\\[\\(?:X\\|\\([0-9]+\\)/\\2\\)\\][^\n]*\n\\)"
     1 'org-checkbox-done-text prepend))
  'append)

(after! org
  (setq org-capture-templates
    '(("d" "Testing Demo" entry (file+headline "agenda.org" "Demo Testing")
       "* TODO %?")
      ("t" "Add file location" entry (file+headline "agenda.org" "Demo")
       "* TODO Fix it: %? \n [[file://%F::%(with-current-buffer (org-capture-get :original-buffer) (number-to-string (line-number-at-pos)))][%f]]")
      ("p" "Capture Project Tasks")
      ("pt" "Add todo" entry (file "project-todos.org")
       "* TODO: %?\n[[file://%F::%(with-current-buffer (org-capture-get :original-buffer) (number-to-string (line-number-at-pos)))][%f]]"))))

(use-package! org
  :hook
  (org-mode . mixed-pitch-mode))
  ;;(org-mode . writeroom-mode))

;; Minor mode for compiling LaTeX automatically
;; Grabbed from -> https://rtime.ciirc.cvut.cz/~sojka/blog/compile-on-save/
;; (defun compile-on-save-start ()
;;   (let ((buffer (compilation-find-buffer)))
;;     (unless (get-buffer-process buffer)
;;       (recompile))))

;; (define-minor-mode compile-on-save-mode
;;   "Minor mode to automatically call `recompile' whenever the
;; current buffer is saved. When there is ongoing compilation,
;; nothing happens."
;;   :lighter " CoS"
;;     (if compile-on-save-mode
;;     (progn  (make-local-variable 'after-save-hook)
;;         (add-hook 'after-save-hook 'compile-on-save-start nil t))
;;       (kill-local-variable 'after-save-hook)))

;; (setq revert-without-query '(".pdf"))
;; (add-hook 'doc-view-mode-hook 'auto-revert-mode);

(add-hook 'svelte-mode-hook 'lsp)

;; Default shell as FISH in vterm
(setq vterm-shell "/usr/bin/fish")

;; Fish completion in eshell
(when (and (executable-find "/usr/bin/fish")
           (require 'fish-completion nil t))
  (global-fish-completion-mode))

;; Add tabnine to company
;; (add-to-list 'company-backends #'company-tabnine)
;; (setq company-idle-delay 0)
;; (setq company-show-numbers t)
;; (setq company-global-modes '(not erc-mode circe-mode message-mode help-mode gud-mode vterm-mode eshell-mode))

;; My Keybinddings
; Keybinds for copying and pasting using system clipboard
(map! :leader
      (:prefix ("y" . "System Clipboard")
       :desc "Copy to XClip" "y" #'copy-to-clipboard
       :desc "Paste from XClip" "p" #'paste-from-clipboard))

; Push to Git Remote
(map! :leader
      :desc "Push to Remote"
      "g p" #'magit-push-current-to-pushremote)

;; (map! :leader
;;       (:prefix ("\\" . "Fuzzy Finder")
;;        :desc "Find file" "f" #'fuzzy-finder
;;        :desc "Go to line" "g" #'fuzzy-finder-goto-gitgrep-line
;;        :desc "Find file in Project" "p" #'fuzzy-finder-find-files-projectile))

(map! :leader
      :desc "Fzf Directory" "\\" #'fzf-directory)

(map! :leader
      :desc "Find file with FZF"
      "p \\" #'fzf-projectile)

(map! :leader
      :desc "Switch Buffer with FZF"
      "b \\" #'fzf-switch-buffer)

;; (defun fzf-find-script-files ()
;;   "Open script files"
;;   (interactive)
;;   ;; (message "hello"))
;;   (let ((d (fzf/resolve-directory "~/.local/scripts")))
;; 	(fzf/start d
;; 	(lambda (x)
;; 	(let ((f (expand-file-name x d)))
;; 		(when (file-exists-p f)
;; 		       find-file f))))))

(defun fzf-find-config-files ()
  "Open script files"
  (interactive)
  (fzf-find-file "~/.config"))

(map! :leader
      :desc "Open config files"
      "f o" #'fzf-find-config-files)

(defun fzf-find-script-files ()
  "Open script files"
  (interactive)
  (fzf-find-file "~/.local/scripts"))

(map! :leader
      :desc "Open scripts"
      "f O" #'fzf-find-script-files)

;; (after! emmet-mode
;;   (map! :i "<tab>" #'emmet-expand-line))
;; Config for Dart-lsp
;(setq lsp-enable-links nil)

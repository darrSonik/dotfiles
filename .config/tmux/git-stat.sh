#!/usr/bin/env bash
git_stat() {
  STATUS="$(git status 2> /dev/null)"
  # if [[ $? -ne 0 ]]; then printf ""; return; else printf " $(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/')"; fi
  if [[ $? -ne 0 ]]; then printf "  "; return; else printf " $(git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ \1/')"; fi
  if echo ${STATUS} | grep -c "nothing to commit"  &> /dev/null; then printf ""; else printf " "; fi
  if echo ${STATUS} | grep -c "renamed:"           &> /dev/null; then printf ">"; else printf ""; fi
  if echo ${STATUS} | grep -c "branch is ahead:"   &> /dev/null; then printf "!"; else printf ""; fi
  if echo ${STATUS} | grep -c "new file:"          &> /dev/null; then printf "+"; else printf ""; fi
  if echo ${STATUS} | grep -c "Untracked files:"   &> /dev/null; then printf "?"; else printf ""; fi
  if echo ${STATUS} | grep -c "modified:"          &> /dev/null; then printf "*"; else printf ""; fi
  if echo ${STATUS} | grep -c "deleted:"           &> /dev/null; then printf "-"; else printf ""; fi
  printf " "
}
git_stat
